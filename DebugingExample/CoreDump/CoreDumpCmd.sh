#this disable coredump, it will only allow coredump with a size smaller than 0 bytes
ulimit -c 0

#this enable coredump and set their max size to unlimited.
#ulimit -c unlimitd
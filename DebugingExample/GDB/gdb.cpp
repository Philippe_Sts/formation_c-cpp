#include "A_Class.h"

int main(int argc, char** argv)
{
    GDB_ExampleClass ex;

    ex.SetData("value1", 55);
    ex.SetData("value2", 35);
    ex.SetData("value3", 65);

    int val = ex.GetData("value3");
    ex.AddToData("value3", val);

    ex.PrintData("value3");
    ex.PrintData("value2");

    ex.ManipulateData();
    ex.PrintData("value3");
    ex.PrintData("value2");
    ex.PrintData("value1");

    return 0;
}
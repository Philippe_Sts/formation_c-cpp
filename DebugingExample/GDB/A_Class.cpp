#include "A_Class.h"
#include <iostream>
#include <algorithm>

GDB_ExampleClass::GDB_ExampleClass()
{

}

GDB_ExampleClass::~GDB_ExampleClass()
{

}

void GDB_ExampleClass::ManipulateData()
{
    for(auto elem: m_Data)
    {
        elem.second++;
    }
}

void GDB_ExampleClass::SetData(const char* name, int value)
{
    if(!SearchData(name))
    {
        m_Data[name] = value;
    }
}

int GDB_ExampleClass::GetData(const char* name)
{
    if(SearchData(name))
    {
        return m_Data[name];
    }
    return 0;
}

void GDB_ExampleClass::PrintData(const char* name)
{
    if(SearchData(name))
    {
        std::cout << "Data at " << name << ": " << m_Data[name] << std::endl;
    }
}

void GDB_ExampleClass::AddToData(const char* name, int value)
{
    if(SearchData(name))
    {
        m_Data[name] += value;
    }
}

bool GDB_ExampleClass::SearchData(const char* name)
{
    std::unordered_map<std::string, int>::const_iterator it = m_Data.find(name);
    return it != m_Data.end();
}
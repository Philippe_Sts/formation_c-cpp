#include <iostream>
#include <string>
#include <string.h>

class MyApplication
{
public:
    MyApplication() = delete;
    MyApplication(bool PassCorrect);
    ~MyApplication();

private:
    void GoodPassword();
    void BadPassword();

    void PrivateFunc();
};

int main(int argc, char** argv)
{
    char password[9] = "passw0rd";
    char username[9];

    std::cin >> username;

    char buffer[9];

    std::cin >> buffer;

    MyApplication App(strcmp(password, buffer) == 0);

    return 0;
}

void MyApplication::PrivateFunc()
{
    std::cout << "Youŕe logged in.\n";
}

MyApplication::MyApplication(bool PassCorrect)
{
    if(PassCorrect)
    {
        GoodPassword();
    } else 
    {
        BadPassword();
    }
}

MyApplication::~MyApplication()
{

}

void MyApplication::GoodPassword()
{
    std::cout << "Welcome!\n";
    PrivateFunc();
}

void MyApplication::BadPassword()
{
     std::cout << "Incorrect password!\n";
}
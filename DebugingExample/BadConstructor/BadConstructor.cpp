#include <iostream>

class Object
{
public:
    Object(int i = 34)
        : m_Val(i)
    {

    }

    ~Object()
    {

    }

    void PrintVal()
    {
        std::cout << m_Val << std::endl;
    }

private:
    Object() = delete;
    
    int m_Val;
};

int main(int argc, char** argv)
{
    Object O(55);
    O.PrintVal();

    return 0;
}
#include <vector>
#include <iostream>

void MemoryLeak()
{
    int* i_ptr = new int;
}

std::vector<int> intVec;

bool MainLoopLongExecutionFct(int value, int& lastValue)
{
    lastValue = rand() % 1000;
    intVec.push_back(lastValue);

    if(lastValue == value)
    {
        return false;
    }
    return true;
}

void FastFct(int& value, int& lastValue)
{
    value += lastValue;
    value %= 1000;
}

void MainLoopFct()
{
    int val = 361;
    int lastValue = val;
    while(MainLoopLongExecutionFct(val, lastValue))
    {
        FastFct(val, lastValue);
    }
}

int main(int argc, char** argv)
{
    MemoryLeak();

    MainLoopFct();

    std::cout << intVec.size() << std::endl;

    return 0;
}
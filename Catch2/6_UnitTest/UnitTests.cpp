#include "UnitTests.h"
#include "UnitTestClass.h"

#ifdef CATCH_CONFIG_MAIN
#include "catch.hpp"

/*
https://mariusbancila.ro/blog/2018/03/29/writing-cpp-unit-tests-with-catch2/
*/

TEST_CASE("Test unit class default constructor")
{
    UnitTest::TestClass tc = UnitTest::TestClass();
    REQUIRE(tc.GetValue() == 50);
    REQUIRE(tc.GetStringValue() == "50");
}

TEST_CASE("Test unit class non-default constructor")
{
    UnitTest::TestClass tc = UnitTest::TestClass(23);
    REQUIRE(tc.GetValue() == 23);
    REQUIRE(tc.GetStringValue() == "23");
}

TEST_CASE("Test unit class setter")
{
    UnitTest::TestClass tc = UnitTest::TestClass(23);
    REQUIRE(tc.GetValue() == 23);
    REQUIRE(tc.GetStringValue() == "23");
    tc.SetValue(31);
    REQUIRE(tc.GetValue() == 31);
    REQUIRE(tc.GetStringValue() == "23");
}

#else

#include <iostream>

#define CUSTOM_ASSERT(x) SimpleAssert(__FILE__, __LINE__, x)

#ifdef __linux__

void SimpleAssert(const char* file, int line, bool assertion)
{
    if(assertion)
    {
        std::cout << "\e[0;32m" << file << ":" << line << " Assertion passed\n";
    } else 
    {
        std::cout  << "\e[0;31m"  << file << ":" << line << " Assertion failed\n";
    }
}

#elif _WIN32

#define NOMINMAX
#include <Windows.h>

void SimpleAssert(const char* file, int line, bool assertion)
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    if(assertion)
    {
        //
        SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
    } else
    {
        SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_INTENSITY);
    }

    std::cout << file << ":" << line << " Assertion passed\n";
    SetConsoleTextAttribute(hConsole, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);
}

#elif __APPLE__

//CODE APPLE

#endif

int main()
{
    CUSTOM_ASSERT(true);
    CUSTOM_ASSERT(false);
}
#endif

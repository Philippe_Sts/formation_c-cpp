#include "MinMax.h"
#include <algorithm>
#include <iostream>

/*
!!!!!
#include <Windows.h>

Windows.h define a macro for min and max. 
so if you want to use std::min and std::max
you'll have to define NOMINMAX

#define NOMINMAX
#include <Windows.h>
*/


namespace MinMax
{
    void MinMaxExample()
    {
        int Min = std::min(5, 7);
        int Max = std::max(5, 7);

        std::cout << "Minimal value is: " << Min << std::endl;
        std::cout << "Maximal value is: " << Max << std::endl;
    }
}
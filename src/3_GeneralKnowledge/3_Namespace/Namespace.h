#pragma once

namespace NS1
{
    void NamespaceFunction();
}

//No collision
namespace NS2
{
    void NamespaceFunction();
}
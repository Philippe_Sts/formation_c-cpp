#include "IO.h"
//system in out
#include <iostream> // C++
#include <stdio.h>  // C

//file in out
#include <fstream>  // C++

namespace IO
{
    void FileIO_Example()
    {
        //Open the file to read
        std::ifstream stream("Assets/data.txt");

        //check if the file exist
        if(stream)
        {
            //read the file line by line
            std::string line;
            while(std::getline(stream, line))
            {
                std::cout << line;
            }
            std::cout << std::endl;
            //close the file
            stream.close();
        }

        //open a file to write
        std::ofstream outStream("Assets/outData.txt");

        if(outStream)
        {
            //write into the file
            outStream << "override first line.\n";
            outStream << "add new line.\n";
            outStream << "add new line.\n";
            outStream << "add new line.\n";

            //close the file
            outStream.close();
        }

        //open a file to write
        //std::ios::app is append mode, it will write at the end of the file.
        std::ofstream outStream2("Assets/outData2.txt", std::ios::app);

        if(outStream2)
        {
            //write into the file
            outStream2 << "add one line to the file.\n";

            //close the file
            outStream2.close();
        }
    }

    void SysIO_Example()
    {
        std::cout << "test output\n";

        char buffer[25];
        std::cin.get(buffer, 25);
        std::cout << buffer << std::endl;
        int i;
        std::cin >> i;
        std::cout << i << std::endl;

        //C style
        printf("print a string\n");
        
        //subject to buffer overflow.
        //scanf(buffer);
    }
}
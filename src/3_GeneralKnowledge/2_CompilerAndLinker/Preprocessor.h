#pragma once

#define DEFINE_SOMETHING 15
#define DEFINE_WITH_ONE_ARG(X) MyFunction(X)
#define DEFINE_WITH_MULTIPLE_ARGS(...) MyFunction(__VA_ARGS__)

#ifdef __linux__
    // Code for linux
#elif _WIN32
    // code for other OS
#endif

#ifdef STANDALONE
    
#else 

#endif
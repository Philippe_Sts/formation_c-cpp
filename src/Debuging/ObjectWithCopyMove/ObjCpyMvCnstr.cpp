#include "ObjCpyMvCnstr.h"
#include <iostream>

int TestObject::CreatedObject = 0;
int TestObject::DestroyObject = 0;
int TestObject::CopyDone = 0;
int TestObject::MoveDone = 0;

TestObject::TestObject()
    : val(0)
{
    //std::cout << "OBJECT CREATED" << std::endl;
    CreatedObject++;
}

TestObject::TestObject(int inVal)
    : val(inVal)
{
    //std::cout << "OBJECT CREATED" << std::endl;
    CreatedObject++;
}

TestObject::TestObject(const TestObject& inVal)
{
    val = inVal.val;
    //std::cout << "COPY DONE" << std::endl;
    CopyDone++;
}

TestObject::TestObject(TestObject&& inVal)
{
    val = inVal.val;
    //std::cout << "MOVE DONE" << std::endl;
    MoveDone++;
}

TestObject::~TestObject()
{
    //std::cout << "OBJECT DESTROY" << std::endl;
    DestroyObject++;
}

void TestObject::PrintObjectStat()
{
    std::cout << "Number of Created: " << CreatedObject
        << "\n" << "Number of Destroyed: " << DestroyObject
        << "\n" << "Number of Copy: " << CopyDone
        << "\n" << "Number of Move: " << MoveDone << std::endl;
}

void TestObject::ResetObjectStat()
{
    CreatedObject = 0;
    DestroyObject = 0;
    CopyDone = 0;
    MoveDone = 0;
}
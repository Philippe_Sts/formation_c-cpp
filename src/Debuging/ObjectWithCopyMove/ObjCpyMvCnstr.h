#pragma once

struct TestObject
{
public:
    //TestObject() = delete;
    TestObject();

    TestObject(int inVal);
    TestObject(const TestObject& inVal);

    TestObject(TestObject&& inVal);

    ~TestObject();

    int val;

    static void PrintObjectStat();
    static void ResetObjectStat();

private:
    static int CreatedObject;
    static int DestroyObject;
    static int CopyDone;
    static int MoveDone;
};
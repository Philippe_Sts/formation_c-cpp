#pragma once
#include <iostream>
#include <chrono>
#include <string>

struct Timer
{
	Timer();

	Timer(const std::string& name);

	~Timer();

	std::chrono::time_point<std::chrono::high_resolution_clock> start, end;
	std::chrono::duration<double> elapsed;
	std::string name;
};
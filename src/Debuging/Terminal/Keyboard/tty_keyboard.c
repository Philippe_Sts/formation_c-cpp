#include "tty_keyboard.h"
#include <pthread.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <linux/input.h>
#include <stdio.h>
#include <string.h>

typedef struct keyboard_key_stateate_st {
    signed short keys[KEY_CNT];
} keyboard_key_stateate;

static pthread_t thread;
static unsigned char active;
static int keyboard_fd;
static struct input_event *keyboard_event;
static keyboard_key_stateate *keyboard_key_state;
static char name[256];
static char event[128];

#define COMMAND_GET_INPUT_DEVICE_EVENT_NBR "grep -E 'Handlers|EV=' /proc/bus/input/devices | grep -B1 'EV=120013' | grep -Eo 'event[0-9]+' | grep -Eo '[0-9]+' | tr -d '\n'"

static void read_event()
{
    int bytes = read(keyboard_fd, keyboard_event, sizeof(*keyboard_event));
    if(bytes > 0)
    {
        if(keyboard_event->type & EV_KEY)
        {
            keyboard_key_state->keys[keyboard_event->code] = keyboard_event->value;
        }
    }
}

static void* keyboard_eventent_loop(void* active)
{
    unsigned char* act = active;
    while(*act)
    {
        read_event();
    }
}

static const char* tty_execute_command(const char* cmd)
{
    FILE *pipe = popen(cmd, "r");
    char buffer[128];
    char* result = malloc(sizeof(char) * 128);

    while (!feof(pipe))
    {
        if(fgets(buffer, 128, pipe) != NULL)
        {
            memcpy(result, buffer, 128);
        }
    }

    const char* event_str = "/dev/input/event";
    strcat(event, event_str);
    strcat(event, result);

    free(result);

    pclose(pipe);

    return event;
}

int tty_start_keyboard()
{
    active = 0;
    keyboard_fd = 0;

    keyboard_event = malloc(sizeof(struct input_event));
    keyboard_key_state = malloc(sizeof(keyboard_key_stateate));

    keyboard_fd = open(tty_execute_command(COMMAND_GET_INPUT_DEVICE_EVENT_NBR), O_RDONLY | O_NONBLOCK);

    if(keyboard_fd > 0)
    {
        ioctl(keyboard_fd, EVIOCGNAME(256), name);

        printf("DEVICE NAME: %s\n", name);

        active = 1;
        pthread_create(&thread, 0, &keyboard_eventent_loop, &active);
    }
}

int tty_stop_keyboard()
{
    if(keyboard_fd > 0)
    {
        active = 0;
        pthread_join(thread, 0);
        close(keyboard_fd);
    }
    free(keyboard_key_state);
    free(keyboard_event);
    keyboard_fd = 0;
}

short tty_get_key_state(unsigned short key)
{
    return keyboard_key_state->keys[key];
}
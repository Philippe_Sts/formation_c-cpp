#include "VariantReturn.h"
#include <variant>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

enum class ErrorCode
{
    NOT_FOUND,
    CORRUPTED,
    NO_ACCESS
};

std::variant<std::string, ErrorCode> VariantReadFile(const char* filePath);

void VariantReturnExample()
{
    std::variant<std::string, int> value;
	value = "string";
	std::cout << std::get<std::string>(value) << std::endl;

	if (value.index() == 0)
		std::string s = std::get<std::string>(value);
	else 
		int i = std::get<int>(value);

	if (auto val = std::get_if<std::string>(&value))
	{
		std::string& str = *val;
	}

    auto var = VariantReadFile("FILENAME");

	if (var.index() == 0)
	{
		std::cout << "SUCCESS \n";
	}
	else
	{
		std::cout << "ERROR \n";
	}

	std::cout << sizeof(int) << "\n";
	std::cout << sizeof(std::string) << "\n";
	std::cout << sizeof(value) << "\n";
}

std::variant<std::string, ErrorCode> VariantReadFile(const char* filePath)
{
    std::ifstream stream(filePath);
    if(stream)
    {
        std::stringstream buffer;
        buffer << stream.rdbuf();
        
        stream.close();
        return buffer.str();
    }
	//RETURN error code
	return ErrorCode::NOT_FOUND;
}
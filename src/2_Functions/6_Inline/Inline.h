#pragma once
#include <iostream>

void InliningExample();

void InlineTranslationUnit2();

/*
Function declared in header must be inline!
*/
//void InlineFunction(const char* str)
inline void InlineFunction(const char* str)
{
    std::cout << str << std::endl;
}

//The same for variables.
inline int InlineVariable = 0;
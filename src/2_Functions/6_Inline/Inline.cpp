#include "Inline.h"

void InlineVar2()
{
    std::cout << InlineVariable << std::endl;
}

void InliningExample()
{
    InlineFunction("Print this string.");

    InlineVar2();

    InlineVariable = 5;

    std::cout << InlineVariable << std::endl;

    InlineVar2();
}
#pragma once
#include <string>
#include <string.h>
#include <iostream>

void TemplateExample();

template<typename T, typename C>
inline void MultipleType(T type1, C type2)
{
    std::cout << type1 << std::endl;
    std::cout << type2 << std::endl;
}

template<typename T>
inline const char* ToString(const T& value)
{
    return "NO TYPE FOUND";
}

template<>
inline const char* ToString<bool>(const bool& val)
{
    if(val)
    {
        return " true ";
    }
    return " false ";
}

template<>
inline const char* ToString<const char*>(const char* const& val)
{
    return val;
}

template<>
inline const char* ToString<int>(const int& val)
{
    return std::to_string(val).c_str();
}

template<>
inline const char* ToString<float>(const float& val)
{
    return std::to_string(val).c_str();
}

template<>
inline const char* ToString<size_t>(const size_t& val)
{
    return std::to_string(val).c_str();
}

template<>
inline const char* ToString<std::string>(const std::string& val)
{
    return val.c_str();
}

template<typename First>
inline void PrintValues(char* buffer, int& index, First&& first)
{
    const char* Str = ToString<First>(first);

    int length = strlen(Str);

    memcpy(&buffer[index], Str, length);

    index += length;
}

template<typename First, typename... Args>
inline void PrintValues(char* buffer, int& index, First&& first, Args&&... args)
{
    const char* Str = ToString<First>(first);

    int length = strlen(Str);

    memcpy(&buffer[index], Str, length);

    index += length;

    if(sizeof...(Args))
    {
        PrintValues(buffer, index, std::forward<Args>(args)...);
    }
}

template<typename... Args>
inline void Print(Args... args)
{
    char buffer[1024 * 10];
    int index = 0;

    PrintValues(&buffer[index], index, std::forward<Args>(args)...);

    buffer[index] = 0;

    std::cout << buffer << std::endl;
}
#include "Scope.h"
#include <iostream>

void FunctionScope(int*& Ptr)
{
    int Value = 5;
    Ptr = &Value;
    (*Ptr)++;
}

void ScopeExample()
{   
    int* Ptr;

    FunctionScope(Ptr);

    std::cout << "Value is: " << *Ptr << std::endl;

    //The scope beging with the { 
    {
        int Value = 5;
        Ptr = &Value;
        (*Ptr)++;
    }
    //Here I'm outside the scope where Value was decalred.
    //So Value has already been destroyed and can't be accessed anymore.
    //Value = 10;

    //The value is correct because the variable was store in the stack and did
    std::cout << "Value is: " << *Ptr << std::endl;

    int Value = 55;
    if(Value == 55)
        std::cout << "this is also a scope" << std::endl;

    while(Value == 55)
    {
        std::cout << "this is also a scope" << std::endl;
        Value++;
    }

    for(int i = 0; i < 5; i++)
        std::cout << "this is also a scope" << std::endl;
}
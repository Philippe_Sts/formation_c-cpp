#include "OptionalReturn.h"
#include <optional>
#include <fstream>
#include <iostream>
#include <sstream>

std::optional<std::string> OptionalReturn(const char* filePath);

void OptionalReturnExample()
{
    std::optional<std::string> datas = OptionalReturn("Assets/data.txt");
    //std::optional<std::string> datas = OptionalReturn("data.txt");
    if(datas)
    {
        std::cout << "Success!\n";
        std::string str = *datas;
        std::cout << str << std::endl;
    } else 
    {
        std::cout << "Failed to open file data.txt.\n";
    }

    std::optional<std::string> datas2 = OptionalReturn("data.txt");
    std::string str = datas2.value_or("something else");
    std::cout << str << std::endl;
}

std::optional<std::string> OptionalReturn(const char* filePath)
{
    std::ifstream stream(filePath);
    if(stream)
    {
        std::stringstream buffer;
        buffer << stream.rdbuf();
        
        stream.close();
        return buffer.str();
    }
    //return empty optional
    return {};
}
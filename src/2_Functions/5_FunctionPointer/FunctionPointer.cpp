#include "FunctionPointer.h"
#include <iostream>
#include <functional>

void PointerToVoidNoARgs();
void PointerToVoidWithArgs(int i, const char* str);
int PointerWithReturnAndArgs(int i, int j, float f, const char* str);

void C_FunctionPtrExample()
{
    void (*FctPtrVoid)() = &PointerToVoidNoARgs;
    void (*FctPtrVoidArgs)(int, const char*) = &PointerToVoidWithArgs;
    int (*FctPtrIntArgs)(int, int, float, const char*) = &PointerWithReturnAndArgs;

    FctPtrVoid();
    FctPtrVoidArgs(3, "custom message to print");

    int val = FctPtrIntArgs(5, 7, 1.6f, "custom message to print");

    std::cout << "PointerWithReturnAndArgs returned: " << val << std::endl;
}

typedef std::function<void(int, const char*)> VoidFct;
#define BIND_VOID_FCT(x) std::bind(&x, std::placeholders::_1, std::placeholders::_2)

typedef std::function<int(int, int, float, const char*)> IntFct;
#define BIND_INT_FCT(x) std::bind(&x, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4)

void Cpp_FunctionPtrExample()
{
    std::function<void()> FctPtrVoid = PointerToVoidNoARgs;

    FctPtrVoid();

    auto FctPtrVoidArgs1 = std::bind(PointerToVoidWithArgs, std::placeholders::_1, "not so custom string");
    FctPtrVoidArgs1(5);

    //This won't work because we need to set the place holder in the right order.
    //We're not able to set the second arg as a place holder if the first arg is not also a placeholder
    //auto FctPtrVoidArgs2 = std::bind(PointerToVoidWithArgs,  std::placeholders::_2, 5);
    //FctPtrVoidArgs2("Custom string");
    //auto FctPtrVoidArgs2 = std::bind(PointerToVoidWithArgs, 5, std::placeholders::_2);
    //FctPtrVoidArgs2("Custom string");

    auto FctPtrVoidArgs3 = std::bind(PointerToVoidWithArgs, std::placeholders::_2, std::placeholders::_1);
    FctPtrVoidArgs3("the string is first", 5);

    VoidFct FctPtrVoidArgs4 = BIND_VOID_FCT(PointerToVoidWithArgs);
    IntFct FctPtrIntArgs = BIND_INT_FCT(PointerWithReturnAndArgs);

    FctPtrVoidArgs4(2, "set with define and typedef");
    int val = FctPtrIntArgs(5, 2, 3.5, "set with typedef and #define");

    std::cout << "FctPtrIntArgs return value is: " << val << std::endl;
}

void PointerToVoidNoARgs()
{
    std::cout << "I print my own message\n";
}

void PointerToVoidWithArgs(int i, const char* str)
{
    while(i >= 0)
    {
        std::cout << str << std::endl;
        i--;
    }
}

int PointerWithReturnAndArgs(int i, int j, float f, const char* str)
{
    std::cout << "Arg_1: " << i
        << " Arg_2: " << j
        << " Arg_3: " << f
        << " Arg_4: " << str
        << std::endl;
    
    return i + j;
}
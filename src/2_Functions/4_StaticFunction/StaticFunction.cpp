#include "StaticFuntion.h"

#include <iostream>

static void StaticFunc()
{
    std::cout << "This can only be called inside StaticFunction.cpp\n";
}

void StaticVarInFunc()
{
    static bool StaticVar = true;
    static int StaticVar2 = 0;

    int NonStaticVar = 5;
    if(StaticVar)
    {
        std::cout << "This is the first execution of this function.\n";
        StaticVar = false;
    }
    StaticVar2++;
    NonStaticVar++;
    std::cout << "Number of execution of this function: " << StaticVar2 << std::endl;
    std::cout << "Non Static var is equal to: " << NonStaticVar << std::endl;
}

void StaticFunctionExample()
{
    StaticFunc();

    for(int i = 0; i < 5; i++)
    {
        StaticVarInFunc();
    }
}

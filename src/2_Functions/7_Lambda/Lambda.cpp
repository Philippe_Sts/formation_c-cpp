#include "Lambda.h"
#include <iostream>
#include <functional>

void Lambda(const std::function<void(int)>& fct);

void LambdaExample()
{
    std::string s = "lambda example";
    int a = 7;

    // not passing any local variable
    Lambda(
        [](int val)
        {
            std::cout << val << std::endl;
        });

    // capture the local variable s as reference
    Lambda(
        [&s](int val)
        {
            std::cout << s << " " << val << std::endl;
        });

    // capture a as value and s by reference
    Lambda(
        [a, &s](int val)
        {
            std::cout << a << " " << s << " " << val << std::endl;
        });
    
    // capture everything as reference
    Lambda(
        [&](int val)
        {
            a = 9;
            std::cout << a << " " << s << " " << val << std::endl;
        });

    // capture everything as value
    Lambda(
        [=](int val)
        {
            //Not possible
            //a = 5;
            std::cout << a << " " << s << " " << val << std::endl;
        });

    // capture everything as value except if it is modified inside the lambda
    Lambda(
        [=](int val) mutable
        {
            //Possible so a is now pass by ref and not by copy via the mutable keyword
            a = 5;
            std::cout << a << " " << s << " " << val << std::endl;
        });
}

void Lambda(const std::function<void(int)>& fct)
{
    fct(5);
}
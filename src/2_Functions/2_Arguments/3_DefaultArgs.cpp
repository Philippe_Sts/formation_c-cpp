#include "3_DefaultArgs.h"
#include <iostream>

void OneDefaultArg(int i = 5);
void MultipleDefaultArgs(int i = 5, float f = 5.0f, const char* c = "unset");

void DefaultArgs()
{
    std::cout << "\n One default arg" << std::endl;
    OneDefaultArg();
    OneDefaultArg(60);

    std::cout << "\n Multiple default args" << std::endl;
    MultipleDefaultArgs();
    MultipleDefaultArgs(60, 54.4f);
    MultipleDefaultArgs(32, 85.9f, "i set the char");
    
    /*
    !!!!!!!
    Warning
    You must set the variable from left to right!
    If you try to set a variable without the previous one
    you'll either :
        be unable to compile
        have unwanted behavior.
    !!!!!!!
    */
    std::cout << "\n Multiple default args with unwanted behavior." << std::endl;
    //Here 3.7f is implicitly cast to int.
    MultipleDefaultArgs(3.7f);
    //MultipleDefaultArgs(2, "this won't compile because you can't cast a const char* to a flaot implicitly.");
}

void OneDefaultArg(int i)
{
    std::cout << "Arg_1: " << i 
        << std::endl;
}

void MultipleDefaultArgs(int i, float f, const char* c)
{
    std::cout << "Arg_1: " << i 
        << " Arg_2: " << f
        << " Arg_3: " << c << std::endl;
}
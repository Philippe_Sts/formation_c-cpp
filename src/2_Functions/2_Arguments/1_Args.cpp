#include "1_Args.h"
#include <iostream>

void FunctionWithOneArg(int i);
void FunctionWithThreeArg(int i, float f, std::string s);

void SameNameFunction(int i, const char* str);
void SameNameFunction(float i, const char* str);
void SameNameFunction(int i, float f, const char* str);

void FunctionArgsExample()
{
    FunctionWithOneArg(15);
    FunctionWithThreeArg(51, 32.5f, "I want to print this string");
    SameNameFunction(1, "I pass only an int.");
    SameNameFunction(1.5f, "I pass only a float.");
    //!!!!
    //This will pose problem, because 1.5 is a double and it can be implicitly cast 
    //to and int or a float, so the compiler and/or linker don't know which function to use.
    //SameNameFunction(1.5, "I pass only a float.");
    //!!!!
    SameNameFunction(1, 2.5f, "I pass an int and a float.");
}

void FunctionWithOneArg(int i)
{
    std::cout << i << std::endl;
}

void FunctionWithThreeArg(int i, float f, std::string s)
{
    std::cout << "Arg_1: " << i 
        << " Arg_2: " << f
        << " Arg_3: " << s << std::endl;
}

void SameNameFunction(int i, const char* str)
{
    std::cout << "Arg_1: " << i 
        << " Arg_2: " << str << std::endl;
}

void SameNameFunction(int i, float f, const char* str)
{
    std::cout << "Arg_1: " << i 
        << " Arg_2: " << f
        << " Arg_3: " << str << std::endl;
}
void SameNameFunction(float i, const char* str)
{
     std::cout << "Arg_1: " << i 
        << " Arg_2: " << str << std::endl;
}
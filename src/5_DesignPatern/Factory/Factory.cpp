#include "Factory.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <memory>

class Product {
public:
    virtual ~Product() {}
    virtual std::string Operation() const = 0;
};

class ConcreteProduct_A : public Product
{
public:
    ConcreteProduct_A(){}
    ~ConcreteProduct_A(){}
    
    virtual std::string Operation() const override
    {
        return "using concrete product A\n";
    }
};

class ConcreteProduct_B : public Product
{
public:
    ConcreteProduct_B(){}
    ~ConcreteProduct_B(){}
    
    virtual std::string Operation() const override
    {
        return "using concrete product B\n";
    }
};

class ConcreteProduct_C : public Product
{
public:
    ConcreteProduct_C(int size, int speed)
        : m_Size(size), m_Speed(speed)
    {}
    ~ConcreteProduct_C(){}
    
    virtual std::string Operation() const override
    {
        return "using concrete product C size : \n";
    }

private:
    ConcreteProduct_C() = delete;
    int m_Size;
    int m_Speed;
};

enum ProductType
{
    PRODUCT_A,
    PRODUCT_B
};

class ProductFactory
{
public:
    ProductFactory(){}
    ~ProductFactory(){}

    std::shared_ptr<Product> GetProduct(char type)
    {
        switch (type)
        {
        case 'A':
            return std::make_shared<ConcreteProduct_A>();
        
        case 'B':
            return std::make_shared<ConcreteProduct_B>();

        case 'C':
            //int size = rand() % 100;
            //int speed = rand() % 100;
            //return std::make_shared<ConcreteProduct_C>(size, speed);
            break;

        default:
            return nullptr;
        }
    }

private:

};

void FactoryMethodExample() {

}
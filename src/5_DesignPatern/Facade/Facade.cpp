#include "Facade.h"
#include <iostream>
#include <string>

class Account
{
public:
    Account(int accountNbr, int securityCode)
        : m_AccountNbr(accountNbr), m_SecurityCode(securityCode)
    {
        m_Funds = 1000;
    }

    ~Account()
    {

    }

    const int GetAccountNumber() const 
    {
        return m_AccountNbr;
    }

    bool CheckSecurityCode(int inputCode) const
    {
        return m_SecurityCode == inputCode;
    }

    const int GetBlance() const 
    {
        return m_Funds;
    }

    void Withdraw(int amount)
    {
        m_Funds -= amount;
    }

    void Deposit(int amount)
    {
        m_Funds += amount;
    }

private:
    int m_AccountNbr;
    int m_SecurityCode;
    int m_Funds;
};

class CheckAccountNumber
{
public:
    CheckAccountNumber(Account* account)
        : m_Account(account)
    {
        
    }

    ~CheckAccountNumber()
    {

    }

    bool CheckBankAccountNumber(int numberToCheck) const
    {
        return m_Account->GetAccountNumber() == numberToCheck;
    }

private:
    Account* m_Account;
};

class CheckSecurityCode
{
public:
    CheckSecurityCode(Account* account)
        : m_Account(account)
    {

    }

    ~CheckSecurityCode()
    {

    }

    bool CheckInputCode(int codeToCheck) const
    {
        if(m_Account->CheckSecurityCode(codeToCheck))
        {
            return true;
        }
        std::cout << "error wrong code.\n";
        return false;
    }

private:
    Account* m_Account;
};

class FundsManager
{
public:
    FundsManager(Account* account)
        : m_Account(account)
    {

    }

    FundsManager()
    {
        
    }

    const int GetCurrentBalance() const 
    {
        std::cout << "Current balance is: " << m_Account->GetBlance() << std::endl;
    }

    bool Withdraw(int cashToWithdraw) 
    {
        if(cashToWithdraw > m_Account->GetBlance())
        {
            std::cout << "Not enough money.\n";
            return false;
        } else 
        {
            m_Account->Withdraw(cashToWithdraw);
            return true;
        }
    }

    void Deposit(int cashToDeposit)
    {
        m_Account->Deposit(cashToDeposit);

        GetCurrentBalance();
    }

private:
    Account* m_Account;
};

class ClientFacade
{
public:
    ClientFacade(int accountNbr, int securityCode)
        : m_CheckSecurityCode(&m_Account), m_CheckAccountNumber(&m_Account), m_FundsManager(&m_Account), m_Account(accountNbr, securityCode)
    {
    }

    ~ClientFacade()
    {
    }

    void GetBalance(int code)
    {
        if(m_CheckSecurityCode.CheckInputCode(code))
        {
            m_FundsManager.GetCurrentBalance();
        }
    }

    void WithdrawCash(int amount, int code)
    {
        if(m_CheckSecurityCode.CheckInputCode(code))
        {
            m_FundsManager.Withdraw(amount);
        }
    }

    void DepositCash(int amount, int code)
    {
        if(m_CheckSecurityCode.CheckInputCode(code))
        {
            m_FundsManager.Deposit(amount);
        }
    }

private:
    Account m_Account;
    CheckSecurityCode m_CheckSecurityCode;
    CheckAccountNumber m_CheckAccountNumber;
    FundsManager m_FundsManager;
};

void FacadeExample() {
    ClientFacade facade(12345678, 1234);

    facade.GetBalance(1234);
    facade.DepositCash(50, 1234);
    facade.GetBalance(1234);
    facade.WithdrawCash(1000, 1234);
    facade.GetBalance(1234);
    facade.WithdrawCash(60, 1234);
    facade.GetBalance(1224);
    facade.GetBalance(1234);
}
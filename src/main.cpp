#include "IncludeExamples.h"

int main(int argc, char** argv)
{
    /*
    ---------------------------
            VARIABLES
    ---------------------------
    */
    //initializationExample();
    //ConstVar();
    //VarReferenceExample();
    //PointerExample();
    //SmartPointerExample();
    //SmartPointerLifeCycle();
    //PrintGlobalVar();
    //AutoType();

    //C_CastExemple();
    //Cpp_CastExemple();

    //C_Arrays();
    //CppArray();
    //CppVectorOptimization();
    //ProblemWithEmplaceBack();
    //MultiDimensionalArray();
    //MultiDimensionalArrayOptimization();

    //C_String();
    //CppString();
    
    //BasicMapExample();
    //MapOrUnorderedMap();
    //MapOrVector();

    //BitField();

    //EnumExample();

    //DequeExample();

    //ListExample();

    //SetExample();

    //ExecSpeedTest();

    /*
    ---------------------------
            FUNCTIONS
    ---------------------------
    */

    //ScopeExample();
    //ReturnExample();
    //FunctionArgsExample();
    //ReferenceExample();
    //DefaultArgs();
    //MultipleReturnValueExample();

    //Can't be called here because it is static.
    //StaticFunc();
    //StaticFunctionExample();
    //C_FunctionPtrExample();
    //Cpp_FunctionPtrExample();
    //InlineTranslationUnit2();
    //InliningExample();
    //InlineTranslationUnit2();
    //LambdaExample();
    //OptionalReturnExample();
    //VariantReturnExample();
    //TemplateExample();

    /*
    ---------------------------
        GENERAL KNOWLEADGE
    ---------------------------
    */

    //int i = AdditionComment(51, 25);
    //L_R_Value::LeftAndRightValueExample();
    //IO::SysIO_Example();
    //IO::FileIO_Example();
    //Sorting::SortArraysExample();
    //MinMax::MinMaxExample();

    /*
    ---------------------------
    OBJECT ORIENTED PROGRAMMING
    ---------------------------
    */

    //Constructor::ConstructorExample();
    //Destructor::DestructorExample();
    //Variables::VariablesExample();
    //Functions::FunctionsExample();
    //Visibility::VisibilityExample();
    //CpyMvConstructor::CopyAndMoveExample();
    //Inheritance::InheritanceExample();
    //Inheritance::VirtualDestructor();
    //Friends::FirendExample();
    //ConstFct::ConstFctExample();
    //OperatorOverride::OperatorOverrideExample();
    //StaticInObject::StaticExample();
    //DynamicCast::OOP_CppCast();
    //ObjectPtr::ObjectPointerExample();
    //Templates::TemplateClassExample();
    //DependencyInjection::DependencyInjectionExample();

    Threading::ThreadingExampleCpp();
}
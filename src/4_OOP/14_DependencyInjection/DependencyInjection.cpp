#include "DependencyInjection.h"

#include <iostream>
#include <unordered_map>
#include <string>
#include <memory>

namespace DependencyInjection
{
    class iDep
    {
    public:
        virtual ~iDep(){}
        virtual void DoSomething() = 0;
    };

    class Dependency_1 : public iDep
    {
    public:
        Dependency_1()
        {

        }

        virtual void DoSomething() override
        {
            std::cout << "Dependency_1 \n";
        }
    };

    class Dependency_2 : public iDep
    {
    public:
        Dependency_2()
        {

        }

        virtual void DoSomething() override
        {
            std::cout << "Dependency_2 \n";
        }
    };

    class DependencyContainer
    {
    public:
        DependencyContainer()
        {

        }

        ~DependencyContainer()
        {

        }

        inline std::shared_ptr<iDep> GetDependency1() 
        {
            if(!SearchDependency("Dependency_1"))
            {
                m_Dependency1Map["Dependency_1"] = std::make_shared<Dependency_1>();
            }
            return m_Dependency1Map["Dependency_1"];
        }

        inline std::shared_ptr<iDep> GetDependency2() 
        {
            if(!SearchDependency("Dependency_2"))
            {
                m_Dependency1Map["Dependency_2"] = std::make_shared<Dependency_2>();
            }
            return m_Dependency1Map["Dependency_2"];
        }

    private:
        bool SearchDependency(const char* str)
        {
            std::unordered_map<std::string, std::shared_ptr<iDep>>::const_iterator got = m_Dependency1Map.find("Value3");
            return got != m_Dependency1Map.end();
        }

        std::unordered_map<std::string, std::shared_ptr<iDep>> m_Dependency1Map;
    };

    class ClassWithDep
    {
    public:
        ClassWithDep(std::shared_ptr<iDep> Dep)
        {
            m_Dependency = Dep;
        }

        ~ClassWithDep()
        {

        }

        void SetDependency(std::shared_ptr<iDep> Dep)
        {
            m_Dependency = Dep;
        }

        void Execute()
        {
            m_Dependency->DoSomething();
        }

    private:
        ClassWithDep() = delete;
        
        std::shared_ptr<iDep> m_Dependency;
    };

    void DependencyInjectionExample()
    {
        DependencyContainer Container;
        ClassWithDep CWD = ClassWithDep(Container.GetDependency1());
        CWD.Execute();
        CWD.SetDependency(Container.GetDependency2());
        CWD.Execute();
    }
}
#include "Destructor.h"
#include <iostream>

namespace Destructor
{
    Object::Object()
    {
        m_f = nullptr;
        std::cout << "Object constructed using default constructor.\n";
    }

    Object::~Object()
    {
        if(m_f)
        {
            delete m_f;
        }
        std::cout << "Object Destroy\n";
    }

    Object::Object(int i, float f)
        : m_i(i), m_f(new float(f))
    {
        //BAD
        //Object();

        //Better way to do
        /*
            -> if the constructors must do the same operations,
               then move those into their own function called by all the constructors
        */
    }

    void DestructorExample()
    {
        {
            Object O1;
            Object O2(52, 2.5f);
        }
    }
}
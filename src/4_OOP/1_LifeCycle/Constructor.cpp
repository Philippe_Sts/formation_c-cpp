#include "Constructor.h"
#include <iostream>

namespace Constructor
{
    //Object functions require the Object::
    Object::Object()
    {
        Value_1 = 1;
        Value_2 = 2.0f;

        std::cout << "Default constructor used" << std::endl;
    }

    Object::Object(int arg_1, float arg_2)
        : Value_1(arg_1), Value_2(arg_2)
    {
        std::cout << "Custom constructor used" << std::endl;
    }

    Object::Object(float arg_1)
    {
        std::cout << "Custom explicit constructor used" << std::endl;
    }

    void ImplicitVsExplicit(const Object& Obj)
    {

    }

    void ConstructorExample()
    {
        Object O1;
        Object O2(7, 2.5f);
        Object O3 = Object(7, 2.5f);

        //Work with an implicit constructor but not with an explicit one.
        ImplicitVsExplicit({ 7, 2.5f });
        //ImplicitVsExplicit(2.5f);
        //Explicit constructor.
        ImplicitVsExplicit(Object(2.5f));
    }
}
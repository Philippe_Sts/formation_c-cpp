#pragma once

namespace Destructor
{
    struct Object
    {
        Object();
        Object(int i, float f);

        //A default destructor is automatically assign but if you need
        //to clean some memory or do some operation before the object is destroyed
        //then use a destructor to do it.
        ~Object();

        int m_i;
        float* m_f;
    };

    void DestructorExample();
} 
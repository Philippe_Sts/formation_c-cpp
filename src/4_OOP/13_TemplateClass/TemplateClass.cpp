#include "TemplateClass.h"
#include <iostream>

namespace Templates
{
    void TemplateClassExample()
    {
        CustomVector<int> Vec;
        Vec.push_back(5);
        Vec.push_back(54);

        Vec[0] = 32;
        std::cout << "\nVec[0] = " << Vec[0] << "\nVec[1] = " << Vec[1] << std::endl;

    }
}
#pragma once
#include <utility>
#include <cstddef>

namespace Templates
{
    template<class T>
    class CustomVector
    {
    public:
        CustomVector();

        ~CustomVector();

        void push_back(T value);

        void pop_back(T value);

        T& operator[] (int index);

    private:
        T* m_Data;
        size_t m_Size;
    };

    template<typename T>
    inline CustomVector<T>::CustomVector()
    {
        m_Size = 0;
        m_Data = new T[m_Size];
    }

    template<typename T>
    inline CustomVector<T>::~CustomVector()
    {
        delete[] m_Data;
    }

    template<typename T>
    inline void CustomVector<T>::push_back(T value)
    {
        m_Size++;
        T* NewData = new T[m_Size];
        for(size_t i = 0; i < m_Size -1; i++)
        {
            NewData[i] = m_Data[i];
        }
        NewData[m_Size - 1] = value;
        delete[] m_Data;
        m_Data = NewData;
    }

    template<typename T>
    inline void CustomVector<T>::pop_back(T value)
    {
        m_Size--;
        delete m_Data[m_Size];
    }

    template<typename T>
    T& CustomVector<T>::operator[] (int index)
    {
        return m_Data[index];
    }

    void TemplateClassExample();
}

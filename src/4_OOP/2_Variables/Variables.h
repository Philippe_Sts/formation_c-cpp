#pragma once
#include <functional>

namespace Variables
{
    struct Object
    {
        Object() = delete;
        Object(int i, float f = 5.2f);
        Object(int i, float f, std::function<void(int, float)> fct);
        ~Object();

        int m_IntVal;
        float m_FloatVal;
        std::function<void(int, float)> m_PrintValues;
    };

    void VariablesExample();
}
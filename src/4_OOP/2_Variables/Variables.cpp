#include "Variables.h"
#include <iostream>

namespace Variables
{
    Object::Object(int i, float f)
        : m_IntVal(i), m_FloatVal(f)
    {

    }

    Object::Object(int i, float f, std::function<void(int, float)> fct)
        : m_IntVal(i), m_FloatVal(f), m_PrintValues(fct)
    {

    }

    Object::~Object()
    {

    }

    void FunctionPrint(int i, float f)
    {
        std::cout << i << " " << f << std::endl;
    }

    void VariablesExample()
    {
        //Object O1;
        Object O1(54);
        if(O1.m_PrintValues)
        {
            O1.m_PrintValues(O1.m_IntVal, O1.m_FloatVal);
        } else 
        {
            std::cout << "No print function passed\n";
        }

        Object O2(54, 5.5f, std::bind(&FunctionPrint, std::placeholders::_1, std::placeholders::_2));
        if(O2.m_PrintValues)
        {
            O2.m_PrintValues(O2.m_IntVal, O2.m_FloatVal);
        } else 
        {
            std::cout << "No print function passed\n";
        }
    }
}
#pragma once

namespace Interface
{
    class iInterface
    {
    public:
        virtual ~iInterface(){}
        /*@{
        Those are called pure virtual function.
        That mean that they don't have a body and must be implemented in the derived class.
        */
        virtual void InterfaceFunc() = 0;
        virtual void InterfacePrintValue() = 0;
        //@}

    protected:
        int m_InterfaceVar;
    };

    class AbstractClass
    {
    public:
        virtual ~AbstractClass(){}
        virtual void AbstractFunc();

        virtual void AbstractPureVirtual() = 0;

    protected:
        int m_AbstractVar;
    };

    class Derived : public iInterface, public AbstractClass
    {
        Derived();
        ~Derived();

        virtual void InterfaceFunc() override;
        virtual void InterfacePrintValue() override;
        virtual void AbstractPureVirtual() override final;
    };
}

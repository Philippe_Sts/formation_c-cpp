#pragma once
#include <vector>

namespace CpyMvConstructor
{
    void CopyAndMoveExample();

    class Object
    {
    public:
        Object();
        Object(size_t size, float value);

        // COPY CONSTRUCTOR
        Object(const Object& inVal);

        // MOVE CONSTRUCTOR
        Object(Object&& inVal);

        ~Object();

        const float GetValueAt(size_t index) const 
        { 
            if(index < m_Size)
            { 
                return m_Array[index]; 
            }
            return 0.0;
        }

    private:
        size_t m_Size;
        float m_BaseNumber;
        float* m_Array;
    };
}
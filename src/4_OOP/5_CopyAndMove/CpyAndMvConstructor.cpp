#include "CpyAndMvConstructor.h"
#include "Debuging/Timing/Timer.h"

#include <math.h>
#include <iostream>

namespace CpyMvConstructor
{
    void Copy(Object O)
    {
        
    }

    void Move(Object O)
    {

    }

    void Reference(Object& O)
    {

    }

    void CopyAndMoveExample()
    {
        Object O1(300, 1.0f);

        //COPY
        Object O_Copy = O1;

        //MOVE
        Object O_Move = std::move(O1);

        //This will segfault because O_Move used the move constructor.
        //Use the move constructor only if you know that you won't be using
        //the object afterward.
        //O1.GetValueAt(50);

        //REFERENCE
        {
            Timer T("Reference");
            Reference(O_Copy);
        }

        //COPY
        {
            Timer T("Copy");
            Copy(O_Copy);
        }

        //MOVE
        {
            Timer T("Move");
            Move(std::move(O_Move));
        }
    }

    Object::Object()
    {

    }

    Object::Object(size_t size, float value)
        : m_Size(size), m_BaseNumber(value)
    {
        m_Array = new float[size];
        for(size_t i = 0; i < size; i++)
        {
            m_Array[i] = log(value * static_cast<float>(i));
        }
    }

    Object::Object(const Object& inVal)
    {
        m_Size = inVal.m_Size;
        m_BaseNumber = inVal.m_BaseNumber;

        m_Array = new float[m_Size];

        for(int i = 0; i < m_Size; i++)
        {
            m_Array[i] = inVal.m_Array[i];
        }
    }

    Object::Object(Object&& inVal)
    {
        m_Size = inVal.m_Size;
        m_BaseNumber = inVal.m_BaseNumber;

        m_Array = inVal.m_Array;

        //the move constructor will destroy the original object.
        //so we need to change the pointer to nullptr
        inVal.m_Array = nullptr;
    }

    Object::~Object()
    {
        std::cout << "Object Destroyed." << std::endl;
        delete[] m_Array;
    }
}
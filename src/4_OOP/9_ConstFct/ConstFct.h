#pragma once

namespace ConstFct
{
    class Object
    {
    public:
        Object();
        ~Object();

        /*
        The first const say that it will return a const int.
        the second const means that we won't be able to change anything
        inside the object in this function.
        */
        const int GetValue() const
        {
            //we can't change anything inside the object due to the second const
            //value = 52;

            return value;
        }

    private:
        int value;
    };

    void ConstFctExample();
}
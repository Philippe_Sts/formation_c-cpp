#include "ConstFct.h"
#include <iostream>

namespace ConstFct
{
    Object::Object()
        : value(25)
    {
        
    }

    Object::~Object()
    {
        
    }

    void ConstFctExample()
    {
        Object O;
        int i = O.GetValue();
        std::cout << i << std::endl;
    }
}
#include "Visibility.h"
#include <iostream>

namespace Visibility
{
    Object::Object()
    {
        std::cout << "Constructed." << std::endl;
    }

    Object::Object(int Int1, int Int2)
        : m_Int(Int1), m_Int2(Int2)
    {
        std::cout << "Constructed." << std::endl;
    }

    Object::~Object()
    {
        std::cout << "Destroyed." << std::endl;
    }

    void Object::ObjFunction()
    {
        std::cout << Add() << std::endl;
    }

    int Object::Add()
    {
        return m_Int2 + m_Int;
    }

    void VisibilityExample()
    {
        Object O1(54, 22);
        O1.ObjFunction();
    }
}
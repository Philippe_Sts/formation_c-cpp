#pragma once

namespace Functions
{
    struct Object
    {
        Object();
        ~Object();

        void Function();

        inline const int GetInt1() const 
        {
            return m_Int;
        }

        int m_Int, m_Int2;
    };

    void FunctionsExample();
}
#include "Functions.h"
#include <iostream>

namespace Functions
{
    Object::Object()
        : m_Int(45), m_Int2(86)
    {

    }

    Object::~Object()
    {

    }

    void Object::Function()
    {
        std::cout << "object content: " << m_Int << " and: " << m_Int2 << std::endl;
    }

    void FunctionsExample()
    {
        Object O1;
        O1.Function();
    }
}
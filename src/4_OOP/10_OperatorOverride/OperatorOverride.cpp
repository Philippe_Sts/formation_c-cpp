#include "OperatorOverride.h"

namespace OperatorOverride
{
    void OperatorOverrideExample()
    {
        Vec2 vec1(2.5f, 5.6f);
        Vec2 vec2(7.4f, 3.1f);

        std::cout << vec1 << std::endl;
        std::cout << vec2 << std::endl;

        ++vec1;
        std::cout << vec1 << std::endl;

        vec1--;
        std::cout << vec1 << std::endl;

        vec1(2.3f, 5.12f);
        std::cout << vec1 << std::endl;

        vec2 += 1.5f;
        std::cout << vec2 << std::endl;

        Vec2 vec3 = vec1 + vec2;

        std::cout << vec3 << std::endl;
        std::cout << vec3[0] << std::endl;

        std::cin >> vec3;
        std::cout << vec3 << std::endl;

        ByteContainer BC(BACK_LIGHT_ON | DIMABLE);
        if(BC & ALL_OFF)
        {
            std::cout << "All flags are off\n";
        } else
        {
            if(BC & BACK_LIGHT_ON)
            {
                std::cout << "Back light is on\n";
            }
            if(BC & DIMABLE)
            {
                std::cout << "Screen light dimable\n";
            }
        }

        unsigned int CurrentFlags = (unsigned int)BC;
    }

    Vec2::Vec2()
        : m_X(0.0f), m_Y(0.0f)
    {

    }

    Vec2::Vec2(float x, float y)
        : m_X(x), m_Y(y)
    {

    }

    Vec2::~Vec2()
    {

    }

    const Vec2 Vec2::Add(const float value) const
    {
        return { m_X + value, m_Y + value };
    }

    const Vec2 Vec2::Substract(const float value) const
    {
        return { m_X - value, m_Y - value };
    }

    const Vec2 Vec2::Multiply(const float value) const
    {
        return { m_X * value, m_Y * value };
    }

    const Vec2 Vec2::Divide(const float value) const
    {
        return { m_X / value, m_Y / value };
    }

    const Vec2 Vec2::Modulo(const float value) const
    {
        return { (int)m_X % (int)value, (int)m_Y % (int)value };
    }

    const Vec2 Vec2::Add(const Vec2& value) const
    {
        return { m_X + value.m_X, m_Y + value.m_Y };
    }

    const Vec2 Vec2::Substract(const Vec2& value) const
    {
        return { m_X - value.m_X, m_Y - value.m_Y };
    }

    const Vec2 Vec2::Multiply(const Vec2& value) const
    {
        return { m_X * value.m_X, m_Y * value.m_Y };
    }

    const Vec2 Vec2::Divide(const Vec2& value) const
    {
        return { m_X / value.m_X, m_Y / value.m_Y };
    }

    const Vec2 Vec2::Modulo(const Vec2& value) const
    {
        return { (int)m_X % (int)value.m_X, (int)m_Y % (int)value.m_Y };
    }

    float& Vec2::operator[] (int index)
    {
        if(index < 2 && index >= 0)
        {
            switch (index)
            {
            case 0:
                return m_X;
            case 1:
                return m_Y;
            
            default:
                break;
            }
        }
    }

    Vec2& Vec2::operator()(float x, float y)
    {
        m_X += x;
        m_Y += y;

        return *this;
    }

    Vec2& Vec2::operator++()
    {
        m_X++;
        m_Y++;

        return *this;
    }

    Vec2 Vec2::operator++(int)
    {
        Vec2 Temp = *this;
        ++*this;
        return Temp;
    }

    Vec2& Vec2::operator--()
    {
        m_X--;
        m_Y--;

        return *this;
    }

    Vec2 Vec2::operator--(int)
    {
        Vec2 Temp = *this;
        --*this;
        return Temp;
    }

    Vec2& Vec2::operator= (const Vec2& val)
    {
        m_X = val.m_X;
        m_Y = val.m_Y;
        return *this;
    }

    Vec2& Vec2::operator= (float val)
    {
        m_X = val;
        m_Y = val;
        return *this;
    }

    Vec2& Vec2::operator= (int val)
    {
        m_X = val;
        m_Y = val;
        return *this;
    }

    Vec2& Vec2::operator= (double val)
    {
        m_X = val;
        m_Y = val;
        return *this;
    }

    Vec2& Vec2::operator+= (const Vec2& val)
    {
        m_X += val.m_X;
        m_Y += val.m_Y;
        return *this;
    }

    Vec2& Vec2::operator+= (float val)
    {
        m_X += val;
        m_Y += val;
        return *this;
    }

    Vec2& Vec2::operator+= (int val)
    {
        m_X += val;
        m_Y += val;
        return *this;
    }

    Vec2& Vec2::operator+= (double val)
    {
        m_X += val;
        m_Y += val;
        return *this;
    }

    Vec2& Vec2::operator-= (const Vec2& val)
    {
        m_X -= val.m_X;
        m_Y -= val.m_Y;
        return *this;
    }

    Vec2& Vec2::operator-= (float val)
    {
        m_X -= val;
        m_Y -= val;
        return *this;
    }

    Vec2& Vec2::operator-= (int val)
    {
        m_X -= val;
        m_Y -= val;
        return *this;
    }

    Vec2& Vec2::operator-= (double val)
    {
        m_X -= val;
        m_Y -= val;
        return *this;
    }

    Vec2& Vec2::operator*= (const Vec2& val)
    {
        m_X *= val.m_X;
        m_Y *= val.m_Y;
        return *this;
    }

    Vec2& Vec2::operator*= (float val)
    {
        m_X *= val;
        m_Y *= val;
        return *this;
    }

    Vec2& Vec2::operator*= (int val)
    {
        m_X *= val;
        m_Y *= val;
        return *this;
    }

    Vec2& Vec2::operator*= (double val)
    {
        m_X *= val;
        m_Y *= val;
        return *this;
    }
    
    Vec2& Vec2::operator/= (const Vec2& val)
    {
        m_X /= val.m_X;
        m_Y /= val.m_Y;
        return *this;
    }
    
    Vec2& Vec2::operator/= (float val)
    {
        m_X /= val;
        m_Y /= val;
        return *this;
    }
    
    Vec2& Vec2::operator/= (int val)
    {
        m_X /= val;
        m_Y /= val;
        return *this;
    }
    
    Vec2& Vec2::operator/= (double val)
    {
        m_X /= val;
        m_Y /= val;
        return *this;
    }
    
    Vec2& Vec2::operator%= (const Vec2& val)
    {
        return *this;
    }
    
    Vec2& Vec2::operator%= (int val)
    {
        return *this;
    }
    
    const Vec2 operator+(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.Add(RightOp);
    }
    
    const Vec2 operator+(const Vec2&  leftOp, int RightOp)
    {
        return leftOp.Add(RightOp);
    }
    
    const Vec2 operator+( const Vec2& leftOp, float RightOp)
    {
        return leftOp.Add(RightOp);
    }
    
    const Vec2 operator+(const Vec2& leftOp, double RightOp)
    {
        return leftOp.Add(RightOp);
    }
    
    const Vec2 operator-(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.Substract(RightOp);
    }

    const Vec2 operator-(const Vec2& leftOp, int RightOp)
    {
        return leftOp.Substract(RightOp);
    }

    const Vec2 operator-(const Vec2& leftOp, float RightOp)
    {
        return leftOp.Substract(RightOp);
    }

    const Vec2 operator-(const Vec2& leftOp, double RightOp)
    {
        return leftOp.Substract(RightOp);
    }

    const Vec2 operator*(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.Multiply(RightOp);
    }

    const Vec2 operator*(const Vec2& leftOp, int RightOp)
    {
        return leftOp.Multiply(RightOp);
    }

    const Vec2 operator*(const Vec2& leftOp, float RightOp)
    {
        return leftOp.Multiply(RightOp);
    }

    const Vec2 operator*(const Vec2& leftOp, double RightOp)
    {
        return leftOp.Multiply(RightOp);
    }

    const Vec2 operator/(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.Divide(RightOp);
    }

    const Vec2 operator/(const Vec2& leftOp, int RightOp)
    {
        return leftOp.Divide(RightOp);
    }

    const Vec2 operator/(const Vec2& leftOp, float RightOp)
    {
        return leftOp.Divide(RightOp);
    }

    const Vec2 operator/(const Vec2& leftOp, double RightOp)
    {
        return leftOp.Divide(RightOp);
    }

    const Vec2 operator%(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.Modulo(RightOp);
    }

    const Vec2 operator%(const Vec2& leftOp, int RightOp)
    {
        return leftOp.Modulo(RightOp);
    }

    bool operator==(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.m_X == RightOp.m_X && leftOp.m_Y == RightOp.m_Y;
    }
    
    bool operator!=(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.m_X != RightOp.m_X && leftOp.m_Y != RightOp.m_Y;
    }
    
    bool operator<(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.m_X < RightOp.m_X && leftOp.m_Y < RightOp.m_Y;
    }
    
    bool operator<=(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.m_X <= RightOp.m_X && leftOp.m_Y <= RightOp.m_Y;
    }
    
    bool operator>(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.m_X > RightOp.m_X && leftOp.m_Y > RightOp.m_Y;
    }
    
    bool operator>=(const Vec2& leftOp, const Vec2& RightOp)
    {
        return leftOp.m_X >= RightOp.m_X && leftOp.m_Y >= RightOp.m_Y;
    }
    
    Vec2* Vec2::operator& ()
    {
        return this;
    }
    
    Vec2* operator&(const Vec2& val)
    {
        return &val;
    }

    std::ostream& operator <<(std::ostream& os, const Vec2& vec)
    {
        os << "Vector2 x: " << vec.m_X << " y: " << vec.m_Y << " ";
        return os;
    }

    std::istream& operator >>(std::istream& is, Vec2& vec)
    {
        is >> vec.m_X >> vec.m_Y;
        return is;
    }

    ByteContainer::ByteContainer()
        : Byte(0)
    {

    }

    ByteContainer::ByteContainer(unsigned char Flags)
        : Byte(Flags)
    {

    }

    ByteContainer::~ByteContainer()
    {

    }

    int ByteContainer::operator&(const ByteContainer& val)
    {
        return Byte & val.Byte;
    }
    
    int operator&(const ByteContainer& leftOp, const ByteContainer& RightOp)
    {
        return leftOp.Byte & RightOp.Byte;
    }
    
    int ByteContainer::operator|(const ByteContainer& val)
    {
        return Byte | val.Byte;
    }
    
    int operator|(const ByteContainer& leftOp, const ByteContainer& RightOp)
    {
        return leftOp.Byte | RightOp.Byte;
    }
    
    int ByteContainer::operator^(const ByteContainer& val)
    {
        return Byte ^ val.Byte;
    }
    
    int operator^(const ByteContainer& leftOp, const ByteContainer& RightOp)
    {
        return leftOp.Byte ^ RightOp.Byte;
    }
    
    int ByteContainer::operator~()
    {
        return ~Byte;
    }

    ByteContainer::operator unsigned char()
    {
        return Byte;
    }

    ByteContainer::operator unsigned int()
    {
        return Byte;
    }
}

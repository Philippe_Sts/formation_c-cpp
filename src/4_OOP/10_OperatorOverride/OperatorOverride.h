#pragma once
#include <iostream>

namespace OperatorOverride
{
    class Vec2
    {
    public:
        Vec2();
        Vec2(float x, float y);
        ~Vec2();

        float& operator[] (int index);

        Vec2& operator()(float x, float y);

        //++var
        Vec2& operator++();
        //var++
        Vec2 operator++(int);

        //--var
        Vec2& operator--();
        //var--
        Vec2 operator--(int);

        Vec2& operator= (const Vec2& val);
        Vec2& operator= (float val);
        Vec2& operator= (int val);
        Vec2& operator= (double val);

        Vec2& operator+= (const Vec2& val);
        Vec2& operator+= (float val);
        Vec2& operator+= (int val);
        Vec2& operator+= (double val);

        Vec2& operator-= (const Vec2& val);
        Vec2& operator-= (float val);
        Vec2& operator-= (int val);
        Vec2& operator-= (double val);

        Vec2& operator*= (const Vec2& val);
        Vec2& operator*= (float val);
        Vec2& operator*= (int val);
        Vec2& operator*= (double val);

        Vec2& operator/= (const Vec2& val);
        Vec2& operator/= (float val);
        Vec2& operator/= (int val);
        Vec2& operator/= (double val);

        Vec2& operator%= (const Vec2& val);
        Vec2& operator%= (int val);

        friend const Vec2 operator+(const Vec2& leftOp, const Vec2& RightOp);
        friend const Vec2 operator+(const Vec2& leftOp, int RightOp);
        friend const Vec2 operator+(const Vec2& leftOp, float RightOp);
        friend const Vec2 operator+(const Vec2& leftOp, double RightOp);

        friend const Vec2 operator-(const Vec2& leftOp, const Vec2& RightOp);
        friend const Vec2 operator-(const Vec2& leftOp, int RightOp);
        friend const Vec2 operator-(const Vec2& leftOp, float RightOp);
        friend const Vec2 operator-(const Vec2& leftOp, double RightOp);

        friend const Vec2 operator*(const Vec2& leftOp, const Vec2& RightOp);
        friend const Vec2 operator*(const Vec2& leftOp, int RightOp);
        friend const Vec2 operator*(const Vec2& leftOp, float RightOp);
        friend const Vec2 operator*(const Vec2& leftOp, double RightOp);

        friend const Vec2 operator/(const Vec2& leftOp, const Vec2& RightOp);
        friend const Vec2 operator/(const Vec2& leftOp, int RightOp);
        friend const Vec2 operator/(const Vec2& leftOp, float RightOp);
        friend const Vec2 operator/(const Vec2& leftOp, double RightOp);

        friend const Vec2 operator%(const Vec2& leftOp, const Vec2& RightOp);
        friend const Vec2 operator%(const Vec2& leftOp, int RightOp);

        friend bool operator==(const Vec2& leftOp, const Vec2& RightOp);
        friend bool operator!=(const Vec2& leftOp, const Vec2& RightOp);
        friend bool operator<(const Vec2& leftOp, const Vec2& RightOp);
        friend bool operator<=(const Vec2& leftOp, const Vec2& RightOp);
        friend bool operator>(const Vec2& leftOp, const Vec2& RightOp);
        friend bool operator>=(const Vec2& leftOp, const Vec2& RightOp);

        //Reference operator
        Vec2* operator& ();
        friend Vec2* operator&(const Vec2& val);

        //Stream operator
        friend std::ostream& operator <<(std::ostream& os, const Vec2& vec);
        friend std::istream& operator >>(std::istream& is, Vec2& vec);
        
        /*
        Vec2* operator->();
        */

    protected:
        const Vec2 Add(const float value) const;
        const Vec2 Substract(const float value) const;
        const Vec2 Multiply(const float value) const;
        const Vec2 Divide(const float value) const;
        const Vec2 Modulo(const float value) const;

        const Vec2 Add(const Vec2& values) const;
        const Vec2 Substract(const Vec2& values) const;
        const Vec2 Multiply(const Vec2& values) const;
        const Vec2 Divide(const Vec2& values) const;
        const Vec2 Modulo(const Vec2& values) const;

        float m_X;
        float m_Y;
    };

    /*
    FLAGS
    */
    #define ALL_OFF         0x00
    #define BACK_LIGHT_ON   0x01
    #define DIMABLE         0x02

    class ByteContainer
    {
    public:
        ByteContainer();
        ByteContainer(unsigned char Flags);
        ~ByteContainer();

        /*
        @{
        Bitwise operator
        */

        //AND
        int operator&(const ByteContainer& val);
        friend int operator&(const ByteContainer& leftOp, const ByteContainer& RightOp);

        //OR
        int operator|(const ByteContainer& val);
        friend int operator|(const ByteContainer& leftOp, const ByteContainer& RightOp);

        //XOR
        int operator^(const ByteContainer& val);
        friend int operator^(const ByteContainer& leftOp, const ByteContainer& RightOp);

        //NOT
        int operator~();
        //@}

        //Here I tell that the cast MUST BE EXPLICIT
        //No implicit cast allowed in this case.
        explicit operator unsigned char();
        explicit operator unsigned int();

    private:
        unsigned char Byte;
    };

    void OperatorOverrideExample();
}
#pragma once

namespace Friends
{
    //Declare that a class named FriendFct exist but don't implement it.
    //It is usefull here because FriendFct need Object and Object need FriendFct
    class Object;

    class FriendFct
    {
    public:
        void CheckObject(Object& obj);
        void ChangeObject(Object& obj);
    };

    //Declare that a class named FriendClass exist but don't implement it.
    //It is usefull here because FriendClass need Object and Object need FriendClass
    class FriendClass;

    class Object
    {
    public:
        Object();
        Object(int val);
        ~Object();

        void PrintPrivateMember();

    private:
        int m_PrivateMember;

        friend FriendClass;

    public:
        friend int FriendFunction(Object& obj);
        friend void FriendFct::ChangeObject(Object& obj);
    };
    
    class FriendClass
    {
    public:
        FriendClass();
        ~FriendClass();
    
        void ChangeObject(Object& obj);
    };

    void FirendExample();
}
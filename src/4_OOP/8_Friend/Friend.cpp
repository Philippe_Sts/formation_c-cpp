#include "Friend.h"
#include <iostream>

namespace Friends
{
    Object::Object()
    {

    }

    Object::Object(int val)
        : m_PrivateMember(val)
    {

    }

    Object::~Object()
    {

    }

    void Object::PrintPrivateMember()
    {
        std::cout << "Private member is equal to: " << m_PrivateMember << std::endl;
    }

    int FriendFunction(Object& obj)
    {
        return obj.m_PrivateMember;
    }

    FriendClass::FriendClass()
    {

    }

    FriendClass::~FriendClass()
    {

    }

    void FriendClass::ChangeObject(Object& obj)
    {
        obj.m_PrivateMember = 53;
    }

    void FriendFct::ChangeObject(Object& obj)
    {
        obj.m_PrivateMember = 33;
    }

    void FriendFct::CheckObject(Object& obj)
    {
        obj.PrintPrivateMember();
        //Can't access the private member.
        //obj.m_PrivateMember;
    }

    void FirendExample()
    {
        Object O1(51);
        O1.PrintPrivateMember();
        FriendClass Frnd;
        FriendFct FndFct;

        Frnd.ChangeObject(O1);
        O1.PrintPrivateMember();

        FndFct.ChangeObject(O1);
        FndFct.CheckObject(O1);

        std::cout << "Friend function: " <<  FriendFunction(O1) << std::endl;
    }
}
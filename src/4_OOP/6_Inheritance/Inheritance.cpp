#include "Inheritance.h"
#include <iostream>

namespace Inheritance
{
    Base::Base()
    {
        std::cout << "Base constructor called\n";
        m_ProtectedVar = 85;
        m_PrivateVar = 54;
        InitFunction();
        PrivateFunction();
    }

    Base::Base(int ProtectedVar)
        : m_ProtectedVar(ProtectedVar)
    {

    }

    Base::~Base()
    {
        std::cout << "Base destructor called\n";
    }

    void Base::BaseFunction()
    {
        std::cout << m_ProtectedVar + m_PrivateVar << std::endl;
    }

    void Base::ProtectedFct()
    {
        std::cout << "Protected var is equal to: " << m_ProtectedVar << std::endl;
    }

    void Base::InitFunction()
    {
        std::cout << "Initialize the base object.\n";
    }

    void Base::PrivateFunction()
    {
        //std::cout << "Only the base class can call me.\n";   
    }

    /*
    You can't initialize member variables declared in base classes, 
    because the base class constructor has already initialized them. 
    All base constructors execute before member constructors.
    */
    Derived::Derived()
    //    : Base(EmptyBaseConstructor())
    //    : m_ProtectedVar(34)
    {
        m_ProtectedVar = 34;

        //unable to access this var because it is private.
        //m_PrivateVar = 12;

        std::cout << "Derived constructor called\n";
        InitFunction();
    }

    Derived::Derived(int ProtectedVar)
        : Base(ProtectedVar)
    {

    }

    Derived::~Derived()
    {
        std::cout << "Derived destructor called\n";
    }

    void Derived::BaseFunction()
    {
        std::cout << m_ProtectedVar << std::endl;
    }


    void Derived::DerivedFunction()
    {
        ProtectedFct();
    }

    void Derived::InitFunction()
    {
        std::cout << "Initialize the derived object.\n";
    }

    void InheritanceExample()
    {
        Derived De;
        De.BaseFunction();
        De.DerivedFunction();

        Derived De2(55);
        De2.DerivedFunction();

        std::cout << std::endl;

        

        std::cout << std::endl;
    }

    void VirtualDestructor()
    {
        Derived* De = new Derived;

        Base* base = De;

        delete base;
    }
}
#pragma once

namespace Inheritance
{
    struct EmptyBaseConstructor {};

    class Base
    {
    public:
        Base();
        Base(int ProtectedVar);

        //Can be  use if you don't want to execute the base constructor code.
        Base(EmptyBaseConstructor) {};

        //This will have undefined behavior, the base destructor might not be called
        //This depends on the system.
        //~Base();

        //It is better to make the destructor virtual so it ensure that the derived destructor is called 
        //when a base pointer object is destroyed.
        virtual ~Base();

        virtual void BaseFunction();

    protected:
        void ProtectedFct();

        virtual void InitFunction();

        int m_ProtectedVar;

    private:
        void PrivateFunction();

        int m_PrivateVar;
    };

    class Derived : public Base
    {
    public:
        Derived();
        Derived(int ProtectedVar);
        ~Derived();

        virtual void BaseFunction() override;

        void DerivedFunction();

    protected:
        virtual void InitFunction() override final;
    };

    class Derived2 : public Derived
    {
    public:
        Derived2();
        Derived2(int ProtectedVar);
        ~Derived2();

    protected:
        // we can't override this function anymore because it was override final in Derived!
        //virtual void InitFunction() override;
    };

    void InheritanceExample();

    void VirtualDestructor();
}
#pragma once
#include <linux/input.h>
#include <memory>

namespace ObjectPtr
{
    void ObjectPointerExample();

    enum class PAGES
    {
        NONE,
        MENU,
        POS_INIT,
        IDENT
    };

    class CDU_Page
    {
    public:
        virtual ~CDU_Page(){}
        virtual bool DrawPage() = 0;

        void ClearDisplay();

        virtual PAGES KeyPressed(unsigned short key) = 0;
    
    protected:
        virtual void UpdateInfo() = 0;
    };

    class MenuPage : public CDU_Page
    {
    public:
        MenuPage();
        ~MenuPage();
        virtual bool DrawPage() override final;

        virtual PAGES KeyPressed(unsigned short key) override;

    protected:
        virtual void UpdateInfo() override final;
    };

    class IdentPage : public CDU_Page
    {
    public:
        IdentPage();
        ~IdentPage();
        virtual bool DrawPage() override final;

        virtual PAGES KeyPressed(unsigned short key) override;

    protected:
        virtual void UpdateInfo() override final;
    };

    class PosInitPage : public CDU_Page
    {
    public:
        PosInitPage();
        ~PosInitPage();
        virtual bool DrawPage() override final;

        virtual PAGES KeyPressed(unsigned short key) override;

    protected:
        virtual void UpdateInfo() override final;
    };

    class CDU
    {
    public:
        CDU();
        ~CDU();

        void Loop();

    private:
        void SwitchPages(PAGES page);
        void DrawPage();

        std::unique_ptr<CDU_Page> m_CurrentPage;
        bool m_PageRequireUpdate;
    };
}
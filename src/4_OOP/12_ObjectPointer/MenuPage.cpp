#include "ObjPointer.h"
#include <iostream>

namespace ObjectPtr
{
    MenuPage::MenuPage()
    {

    }

    MenuPage::~MenuPage()
    {

    }

    bool MenuPage::DrawPage()
    {
        ClearDisplay();
        std::cout << R"(
                      MENU
                             efis cp
            <FMC             SELECT>
                            eicas cp
            <ACARS           SELECT>
                             ctl pnl
                             off-ON
            <ACMS
            <CMC
        )" << std::endl;

        return false;
    }

    PAGES MenuPage::KeyPressed(unsigned short keycode)
    {
        switch (keycode)
        {
        case KEY_P:
            return PAGES::POS_INIT;
        //case KEY_M:
        //    return PAGES::MENU;
        case KEY_I:
            return PAGES::IDENT;
        
        default:
            return PAGES::NONE;
        }
    }

    void MenuPage::UpdateInfo()
    {

    }
}
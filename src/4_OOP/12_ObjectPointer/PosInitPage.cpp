#include "ObjPointer.h"
#include <iostream>

namespace ObjectPtr
{
    PosInitPage::PosInitPage()
    {

    }

    PosInitPage::~PosInitPage()
    {

    }
    
    bool PosInitPage::DrawPage()
    {
        ClearDisplay();
        std::cout << R"(
                   POS INIT         
                               1/3 
                           last pos
                  N50°54.3 E004°29.4
             ref airport
            ----
             gate
            -----
                             gps pos
                   N50°54.3 E004°29.4
                         set irs pos

            -------------------------
            <INDEX             ROUTE>
        )" << std::endl;

        return false;
    }

    PAGES PosInitPage::KeyPressed(unsigned short keycode)
    {
        switch (keycode)
        {
        //case KEY_P:
        //    return PAGES::POS_INIT;
        case KEY_M:
            return PAGES::MENU;
        case KEY_I:
            return PAGES::IDENT;
        
        default:
            return PAGES::NONE;
        }
    }

    void PosInitPage::UpdateInfo()
    {

    }
}
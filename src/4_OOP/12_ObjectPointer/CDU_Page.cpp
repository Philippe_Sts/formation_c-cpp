#include "ObjPointer.h"
#include "Debuging/Terminal/Screen/tty_screen.h"

namespace ObjectPtr
{
    void CDU_Page::ClearDisplay()
    {
        tty_clear_screen();
    }
}
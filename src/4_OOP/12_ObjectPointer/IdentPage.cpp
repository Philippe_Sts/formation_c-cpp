#include "ObjPointer.h"
#include <iostream>

namespace ObjectPtr
{
    IdentPage::IdentPage()
    {

    }

    IdentPage::~IdentPage()
    {

    }
    
    bool IdentPage::DrawPage()
    {
        ClearDisplay();
        std::cout << R"(
                    IDENT            
             model          engines
            747-400         -80C2B1F
             nav data        active
            BA12010001 JAN12FEB10/21
             drag / ff
            
             op program     co data
            AW-P010-80        BA1001
             opc
            AW-C010-80
            ------------------------
            <INDEX         POS INIT>
        )" << std::endl;

        return false;
    }

    PAGES IdentPage::KeyPressed(unsigned short keycode)
    {
        switch (keycode)
        {
        case KEY_P:
            return PAGES::POS_INIT;
        case KEY_M:
            return PAGES::MENU;
        //case KEY_I:
        //    return PAGES::IDENT;
        
        default:
            return PAGES::NONE;
        }
    }

    void IdentPage::UpdateInfo()
    {

    }
}
#include "ObjPointer.h"
#include <memory>
#include "Debuging/Terminal/Keyboard/tty_keyboard.h"


namespace ObjectPtr
{
    CDU::CDU()
    {
        m_CurrentPage = std::make_unique<MenuPage>();
        m_PageRequireUpdate = true;
    }

    CDU::~CDU()
    {

    }

    void CDU::Loop()
    {
        while(1)
        {
            if(tty_get_key_state(KEY_P))
            {
                SwitchPages(m_CurrentPage->KeyPressed(KEY_P));
            }
            if(tty_get_key_state(KEY_M))
            {
                SwitchPages(m_CurrentPage->KeyPressed(KEY_M));
            }
            if(tty_get_key_state(KEY_I))
            {
                SwitchPages(m_CurrentPage->KeyPressed(KEY_I));
            }
            if(tty_get_key_state(KEY_C))
            {
                break;
            }
            DrawPage();         
        }
    }

    void CDU::SwitchPages(PAGES page)
    {
        switch (page)
        {
        case PAGES::IDENT:
            m_CurrentPage = std::make_unique<IdentPage>();
            break;

        case PAGES::MENU:
            m_CurrentPage = std::make_unique<MenuPage>();
            break;
        case PAGES::POS_INIT:
            m_CurrentPage = std::make_unique<PosInitPage>();
            break;
        
        default:
            break;
        }
        m_PageRequireUpdate = true;
    }

    void CDU::DrawPage()
    {
        if(m_PageRequireUpdate)
        {
            m_PageRequireUpdate = m_CurrentPage->DrawPage();
        }
    }

    void ObjectPointerExample()
    {
        tty_start_keyboard();
        CDU cdu;
        cdu.Loop();
        tty_stop_keyboard();
    }
}
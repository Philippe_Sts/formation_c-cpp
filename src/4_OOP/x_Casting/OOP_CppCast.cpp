#include "OOP_CppCast.h"
#include <iostream>

namespace DynamicCast
{
    Base::Base()
        : m_Val_1(0), m_Val_2(0), m_Val_3(0)
    {
        std::cout << "Base default constructor called\n";
    }

    Base::Base(int val1, int val2, int val3)
        : m_Val_1(val1), m_Val_2(val2), m_Val_3(val3)
    {
        std::cout << "Base constructor with args called\n";
    }

    Base::~Base()
    {

    }

    void Base::VirtualFunction()
    {
        std::cout << "Values addition is equal to: " << m_Val_1 + m_Val_2 + m_Val_3 << std::endl; 
    }

    void Base::BaseFunction()
    {
        std::cout << "m_Val_1: " << m_Val_1 << " m_Val_2: " << m_Val_2 << " m_Val_3 " << m_Val_3 << std::endl;
    }

    Derived::Derived()
    {

    }

    Derived::Derived(int val1, int val2, int val3)
        : Base(val1, val2, val3)
    {
        
    }

    Derived::~Derived()
    {

    }

    void Derived::VirtualFunction()
    {
        std::cout << "Values substraction is equal to: " << m_Val_1 - m_Val_2 - m_Val_3 << std::endl;
    }

    void Derived::DerivedFunction()
    {
        std::cout << "This is the derived function.\n";
    }
    
    void OOP_CppCast()
    {
        Derived DerivedObj(5, 52, 61);
        Base* CastVar = dynamic_cast<Base*>(&DerivedObj);

        //Will call the derived implementation.
        CastVar->VirtualFunction();
        DerivedObj.VirtualFunction();

        CastVar->BaseFunction();

        //Won't compile because Derived function is not a Base function.
        //CastVar->DerivedFunction();
        DerivedObj.DerivedFunction();

        Base BaseObj(5, 52, 61);
        //Compilation warning
        Derived* CastVar2 = dynamic_cast<Derived*>(&BaseObj);

        BaseObj.VirtualFunction();
        CastVar->VirtualFunction();

        //This won't work because the compiler will check if the function is implemented.
        //CastVar2->DerivedFunction();

        // This won't compile because Other class has nothing to do with Base
        //OtherClass Other;
        //Base* CastVar3 = dynamic_cast<Base*>(&Other);
    }
}
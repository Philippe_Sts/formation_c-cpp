#pragma once


namespace DynamicCast
{
    class Base
    {
    public:
        Base();
        Base(int val1, int val2, int val3);
        virtual ~Base();

        virtual void VirtualFunction();
        void BaseFunction();

    protected:
        int m_Val_1, m_Val_2, m_Val_3;
    };

    class Derived : public Base
    {
    public:
        Derived();
        Derived(int val1, int val2, int val3);
        ~Derived();

        virtual void VirtualFunction() override;

        void DerivedFunction(); 
    };

    class OtherClass
    {
    public:
        OtherClass(){}
    };

    void OOP_CppCast();
}
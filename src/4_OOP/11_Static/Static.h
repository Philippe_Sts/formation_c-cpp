#pragma once

namespace StaticInObject
{
    void StaticExample();

    class StaticObject
    {
    public:
        StaticObject() = delete;
        StaticObject(int i);
        ~StaticObject();

        static void StaticFunction();

    private:
        //STATIC "MEMBERS" SHOULD NEARLY ALWAYS BE PRIVATE!!
        static int s_Val;

        int m_Val;

    };
}
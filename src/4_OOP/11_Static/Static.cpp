#include "Static.h"
#include <iostream>

namespace StaticInObject
{
    //We must initialize static variable.
    //Because they are global variables.
    int StaticObject::s_Val = 0;

    /*
    In most cases this will be a more proper way to deal with global variables.
    Tend to use static only if:
        - You want the static variable to be inherited by derived class
        - You split the implementation of the object into multiple files and all those files needs to have access to that global variable.

    Both of these cases are very unlikely and are to be avoid as much as you can.
    
    If you want to share global variables between multiple functions in the same file then use the unnamed namespace.

    If you need to share global variables between multiple c functions implemented in multiple files, then be careful.
    If you need to share global variables between multiple objects, then pass a struct reference that contains the variables instead of making them global.
    */
    namespace
    {
        int FileScopedValue = 0;
    }

    //s_Val does not work because this initialize at object creation,
    //But s_Val is static and so is a global variable.
    StaticObject::StaticObject(int i)
    //    : s_Val(i)
        : m_Val(i)
    {
        //This will be allowed.
        std::cout << i << std::endl;
        s_Val = 5 * i;
    }

    StaticObject::~StaticObject()
    {

    }

    //Static functions is basicaly a C-style function.
    //It won't be able to access or modify non static members.
    //This function is a C style function and is thus not linked to any of the objects.
    void StaticObject::StaticFunction()
    {
        //m_Val = 236;
        //std::cout << m_Val + s_Val << std::endl;

        std::cout << s_Val << std::endl;
    }

    void StaticExample()
    {
        StaticObject::StaticFunction();

        //This won't compile because the default constructor is deleted.
        //StaticObject S_Obj;
        //This work because the constructor has a default value.
        StaticObject S_Obj(55);

        StaticObject::StaticFunction();
    }
}
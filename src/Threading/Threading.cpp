#include "Threading.h"
#include <functional>
#include <future>
#include <thread>
#include <atomic>
#include <mutex>
#include <iostream>
#include <unistd.h>
#include <vector>
#include <deque>
#include <atomic>
#include <condition_variable>
#include <chrono> // required for chrono literal

namespace Threading
{
    class AsyncClass
    {
    public:
        AsyncClass()
        {
            std::cout << "constructed!\n";
            m_Threads.push_back(std::async(std::launch::async, &AsyncClass::AsyncFunction_1, this, 5));
            m_Threads.push_back(std::async(std::launch::async, &AsyncClass::AsyncFunction_2, this, 5));
        }

        ~AsyncClass()
        {
            for(int i = 0; i < m_Threads.size(); i++)
            {
                std::cout << "Return value is equal to: " << m_Threads[i].get() << std::endl;
            }
        }

    private:
        int AsyncFunction_1(int loopNbr)
        {
            for(int j = 0; j < loopNbr; j++)
            {
                for(int i = 0; i < 3; i++)
                {
                    std::unique_lock<std::mutex> lock(m_LockString);
                    m_Output += " thread1 " + std::to_string(i);
                    lock.unlock();
                    usleep(125);
                }
                std::cout << "\n" << m_Output << "\n\n";
                m_Output = "";
            }
            return 1;
        }

        int AsyncFunction_2(int loopNbr)
        {
            for(int j = 0; j < loopNbr; j++)
            {
                for(int i = 0; i < 3; i++)
                {
                    std::unique_lock<std::mutex> lock(m_LockString);
                    m_Output += " thread2 " + std::to_string(i);
                    lock.unlock();
                    usleep(150);
                }
                std::cout << "\n" << m_Output << "\n\n";
                m_Output = "";
            }
            return 2;
        }

        std::vector<std::future<int>> m_Threads;
        std::atomic<bool> m_ThreadActive;
        std::mutex m_LockString;
        std::string m_Output;
    };

    static std::string AsyncFunction(int arg_1, const char* path)
    {
        for(int i = 0; i < arg_1; i++)
        {
            std::cout << "Async thread\n";
            sleep(1);
        }
        std::cout << "ASYNC TASK DONE!" << std::endl;
        return "async returned value";
    }

    static void ThreadFunction(int arg_1, int arg_2)
    {
        for(int i = 0; i < arg_2; i++)
        {
            int result = 1;
            for(int i = arg_1; i > 1; i--)
            {
                result *= i;
            }   
            std::cout << "Result : " << result << std::endl;
            sleep(1);
        }
    }

    class ThreadPool
    {
    public:
        ThreadPool()
        {
            size_t MaxThreads = std::thread::hardware_concurrency();
            if(MaxThreads > 5)
            {
                m_ThreadPoolActive = true;

                for(int i = 0; i < 5; i++)
                {
                    m_Worker.push_back(std::thread(&ThreadPool::WorkerLoop, this));
                }
            }
        }

        ~ThreadPool()
        {
            m_ThreadPoolActive = false;
            m_Condition.notify_all();
            for(int i = 0; i < 5; i++)
            {
                m_Worker[i].join();
            }
            m_Worker.clear();
        }

        void PushJob(std::function<void()> job)
        {
            std::cout << "Pushed job\n";
            m_Queue.push_back(job);
            m_Condition.notify_all();
        }

    private:
        void WorkerLoop()
        {
            std::function<void()> job;
            while(1)
            {
                {
                    /*
                    unique_lock required for .wait()
                    */
                    std::unique_lock<std::mutex> lock(m_QueueLock);

                    std::cout << "WAIT\n";
                    m_Condition.wait(lock, [=]{ return !this->m_Queue.empty() || !this->m_ThreadPoolActive; });
                    
                    if(!m_ThreadPoolActive)
                    {
                        break;
                    }

                    job = m_Queue.front();
                    m_Queue.pop_front();
                }
                job();
            }
        }

        std::vector<std::thread> m_Worker;
        std::deque<std::function<void()>> m_Queue;
        std::mutex m_QueueLock;
        std::condition_variable m_Condition;
        std::atomic<bool> m_ThreadPoolActive;
    };

    void Job()
    {
        size_t sleepTime = rand() % 5;
        sleep(sleepTime);

        std::cout << "Job done!\n";
    }

    void ThreadingExampleCpp()
    {
        
        AsyncClass* AsyncCls = new AsyncClass();

        std::future<std::string> AsynFct = std::async(std::launch::async, AsyncFunction, 5, "Assets/data.txt");

        std::thread thread1(ThreadFunction, 5, 10);

        /*
        using namespace std is to be avoided but you can some using namespace inside a function.
        */
        using namespace std::chrono_literals;

        std::future_status status = AsynFct.wait_for(0ms);

        if(status != std::future_status::ready)
        {
            std::cout << "async thread still running\n";
        }

        for(int i = 0; i < 12; i++)
        {
            std::cout << "Main thread\n";
            sleep(1);
        }

        thread1.join();
        std::cout << AsynFct.get() << std::endl;
        delete AsyncCls;
        

        ThreadPool thdPool;

        thdPool.PushJob(std::bind(Job));
        
        thdPool.PushJob(std::bind(Job));
        thdPool.PushJob(std::bind(Job));
        thdPool.PushJob(std::bind(Job));
        thdPool.PushJob(std::bind(Job));

        sleep(5);
        std::cout << "main done\n";
    }
}
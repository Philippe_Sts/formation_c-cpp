#include "Maps.h"
#include <map>
#include <unordered_map>
#include <string>
#include <iostream>
#include <vector>

#include "Debuging/Timing/Timer.h"

void BasicMapExample()
{
    std::map<std::string, int> Map;

    Map["Value1"] = 59;
    Map["Value2"] = 89;

    //Search if something already exsit
    std::map<std::string, int>::const_iterator got = Map.find("Value3");
    if(got == Map.end())
    {
        std::cout << "Map does not have a key \"Value3\"" << std::endl;
        Map["Value3"] = 100;
    } else 
    {
        std::cout << "Map does have a key \"Value3\"" << std::endl;
    }

    std::unordered_map<std::string, int> UMap;
    UMap["Value1"] = 59;
    UMap["Value2"] = 89;
    UMap["Value3"] = 100;

    //Search if something already exsit
    std::unordered_map<std::string, int>::const_iterator got2 = UMap.find("Value3");
    if(got2 == UMap.end())
    {
        std::cout << "Map does not have a key \"Value3\"" << std::endl;
        UMap["Value3"] = 100;
    } else 
    {
        std::cout << "Map does have a key \"Value3\"" << std::endl;
    }

    //Change a value
    UMap["Value3"] = 342;

    //Cpp98
    /*
    for(std::map<std::string, int>::iterator it = Map.begin(); it != Map.end(); it++)
    {
        std::cout << "Key is: " << it->first << " Val is: " << it->second << std::endl;
    }
    */
    //Cpp11
    /*
    for(const auto& keyVal: UMap)
    {
        std::cout << "Key is: " << keyVal.first << " Val is: " << keyVal.second << std::endl;
    }
    */
    //Cpp 17
    for(const auto& [key, val]: UMap)
    {
        std::cout << "Key is: " << key << " Val is: " << val << std::endl;
    }

    for(auto& [key, val]: UMap)
    {
        val++;
    }

    for(const auto& [key, val]: UMap)
    {
        std::cout << "Key is: " << key << " Val is: " << val << std::endl;
    }

    std::cout << "Value at Key \"Value3\" is: " << UMap["Value3"] << std::endl;

    //Remove an element
    UMap.erase("Value3");
}

void MapOrUnorderedMap()
{
    {
        Timer T("std::map");
        std::map<std::string, int> Map;
        for(int i = 0; i < 2000; i++)
        {
            Map["Value"+std::to_string(i)] = i;
        }

        std::cout << Map["Value3"] << std::endl;
    }

    {
        Timer T("std::unordered_map");
        
        std::unordered_map<std::string, int> UMap;
        
        for(int i = 0; i < 2000; i++)
        {
            UMap["Value"+std::to_string(i)] = i;
        }

        std::cout << UMap["Value3"] << std::endl;
    }
}

#define _SIZES 2000
#define VALUE_TO_GET "Value70"
#define VECT_VALUE_TO_GET 70

void MapOrVector()
{
    {
        std::unordered_map<std::string, int> UMap;
        
        {
            Timer T("std::unordered_map write");
            for(int i = 0; i < _SIZES; i++)
            {
                UMap["Value"+std::to_string(i)] = i;
            }
        }

        {
            Timer T("std::unordered_map itteration");

            int ret;
            for(int Task = 0; Task < 2000; Task++)
            {
                for(auto [key, val] : UMap)
                {
                    if(val == VECT_VALUE_TO_GET)
                    {
                        ret = val;
                        break;
                    }
                }
            }
            std::cout << ret << std::endl;
        }

        Timer T("std::unordered_map read");

        int ret;
        for(int Task = 0; Task < 2000; Task++)
        {
            //UMap[VALUE_TO_GET]++;
            ret = UMap[VALUE_TO_GET];
        }
        std::cout << ret << std::endl;
    }

    std::cout << std::endl;
    
    {
        std::vector<int> Vect;
        Vect.reserve(_SIZES);
        
        {
            Timer T("std::vector write");
            for(int i = 0; i < Vect.size(); i++)
            {
                Vect.emplace_back(i);
            }
        }

        Timer T("std::vector read");
        
        int ret;
        for(int Task = 0; Task < 2000; Task++)
        {
            for(int i = 0; i < Vect.size(); i++)
            {
                if(Vect[i] == VECT_VALUE_TO_GET)
                {
                    //Vect[i]++;
                    ret = Vect[i];
                    break;
                }
            }
        }
        std::cout << ret << std::endl;
    }
}
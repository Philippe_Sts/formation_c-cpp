#include "Pointer.h"
#include <iostream>

struct TestStruct
{
    int value_1;
    int value_2;
};

void PointerExample()
{
    //Assign the require space in memory
    int *C_Pointer = (int*)malloc(sizeof(int));

    //Assign a value that will be store at the pointing address
    *C_Pointer = 10;

    std::cout << std::endl;
    std::cout << "Ptr value is: " << C_Pointer << std::endl;
    std::cout << "Ptr pointing vallue is: " << *C_Pointer << std::endl;
    std::cout << std::endl;
    
    //Free the memory to avoid memory leak.
    free(C_Pointer);

    //Assign the require space in memory
    C_Pointer = new int;

    //Assign a value that will be store at the pointing address
    *C_Pointer = 10;

    std::cout << std::endl;
    std::cout << "Ptr value is: " << C_Pointer << std::endl;
    std::cout << "Ptr pointing vallue is: " << *C_Pointer << std::endl;
    std::cout << std::endl;
    
    //Free the memory to avoid memory leak.
    delete C_Pointer;

    int Val = 123;
    int* PtrToVal = &Val;

    std::cout << std::endl;
    std::cout << "Val memory address is: " << &Val << std::endl;
    std::cout << "Ptr value is: " << PtrToVal << std::endl;
    std::cout << "Ptr pointing value is: " << *PtrToVal << std::endl;
    std::cout << std::endl;

    /*!!! DO NOT DELETE else you'll try to remove val from memory and have an 
    // "munmap_chunk(): invalid pointer" or "free(): invalid pointer"
    // Error
    //eg: (uncomment to execute)*/
    //delete PtrToVal;

    /*
    Segmentation fault example.
    !!!!This will only work in release mode!!!!
    In debug mode the code will run normally because it assign default value.

    Uncomment and compile in release and debug mode the following code.
    */
    /*
    int* SegFaultPtr;

    std::cout << "Ptr value is: " << SegFaultPtr << std::endl;
    std::cout << "Ptr pointing vallue is: " << *SegFaultPtr << std::endl;
    */

    /*
    Memory leak example.
    Use valgrind
    */
    /*
    int* MemoryLeak = new int;

    std::cout << "Ptr value is: " << MemoryLeak << std::endl;
    std::cout << "Ptr pointing vallue is: " << *MemoryLeak << std::endl;
    */

    /*
    !!!! WARNING !!!!
    */
    int Value = 453;
    int* OperationOnPtr = &Value;

    std::cout << "Ptr value is: " << OperationOnPtr << std::endl;
    std::cout << "Ptr pointing vallue is: " << *OperationOnPtr << std::endl;

    OperationOnPtr++;
    std::cout << "Ptr value is: " << OperationOnPtr << std::endl;
    std::cout << "Ptr pointing vallue is: " << *OperationOnPtr << std::endl;

    TestStruct* StructPtr = new TestStruct;
    StructPtr->value_1 = 5;
    (*StructPtr).value_2 = 6;
}
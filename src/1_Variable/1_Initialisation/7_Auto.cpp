#include "Auto.h"
#include <iostream>

//Use here to know the type of a variable
#include <typeinfo>

void AutoType()
{
    float Variable1 = 6.54f;
    auto AutoVar = Variable1;

    std::cout << "\n"
        << "Auto variable value is: " << AutoVar << "\n"
        << "Auto variable type is: " << typeid(AutoVar).name() << "\n";

    auto AutoVar2 = 1.0f;

    std::cout << "\n"
        << "Auto variable value is: " << AutoVar2 << "\n"
        << "Auto variable type is: " << typeid(AutoVar2).name() << "\n";

    int IntVal = 83921;
    AutoVar2 = IntVal;

    std::cout << "\n"
        << "Auto variable value is: " << AutoVar2 << "\n"
        << "Auto variable type is: " << typeid(AutoVar2).name() << "\n";

    int& refVal = IntVal;
    auto AutoVar3 = refVal;
    AutoVar3 = 5;
    std::cout << "\n"
        << "Auto variable int value is: " << IntVal << "\n"
        << "Auto variable value is: " << AutoVar3 << "\n"
        << "Auto variable type is: " << typeid(AutoVar3).name() << "\n";
}
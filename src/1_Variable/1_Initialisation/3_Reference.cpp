#include "Reference.h"
#include <iostream>

void VarReferenceExample()
{
    int i = 9;
    int& ref_i = i;

    ref_i += 12;

    i += 5;

    std::cout << ref_i << std::endl;
}
#include "Global.h"

#include <iostream>

/*
Global variables are usually bad,
So try to avoid them as much as you can.

One case where a global variable can be justify is if it is a constant.
in this case you can declare it using:
    #define NAME 0
    const type NAME = 0;

This would be the same for the compiler.
*/

int GlobVar = 11;

//Declaration and definition of a global variable.
//This variable can only be use by functions inside this file.
int SourceFileGlobVar = 24;

/*
This variable is scoped to this translation unit (the file)
It is global to the file and can be shared between it's functions.
But it is not global because it can't be accessed by other translation units.
*/
static int FileScopeVariable = 523;

/*
Using an unnamed namespace is exactly the same than using the static above.
*/
namespace
{
    int UnnamedNamespaceVar = 325;
}

void PrintGlobalVar()
{
    std::cout << "\n" << GlobVar << "\n";
    std::cout << "\n" << GlobalConstVar << "\n";
}

void PrintSourceFileGlobalVar()
{
    std::cout << "\n" << SourceFileGlobVar << "\n";
}
#include "Strings.h"

#include <string>
#include <cstring>
#include <iostream>

void C_String()
{
    const char* c_str1 = "This is a string.";
    const char* c_str2 = "This is a string.";
    const char* c_str3 = "This is an other string.";

    if(strcmp(c_str1, c_str2) == 0)
    {
        std::cout << "c_str1 is equal to c_str2 \n";
    }

    if(strcmp(c_str1, c_str3) == 0)
    {
        std::cout << "c_str1 is equal to c_str3 \n";
    } else
    {
        std::cout << "c_str1 is not equal to c_str3 \n";
    }

    size_t C_StrSize = strlen(c_str1);
    std::cout << "c_str1 len: " << C_StrSize << std::endl;

    //char at
    std::cout << "c_str1 char at 2: " << c_str1[2] << std::endl;

    //copy
    //Create a buffer in memory to copy the string to.
    char* c_Ptr = (char*)malloc(sizeof(char) * C_StrSize);
    //copy the string into that buffer.
    strcpy(c_Ptr, c_str1); // !!!unsafe!!!
    //or
    strncpy(c_Ptr, c_str1, C_StrSize); // safer

    std::cout << "c_Ptr copy: " << c_Ptr << std::endl;
}

void CppString()
{
    std::string CppStr1 = "This is a std::string";
    std::string CppStr2 = "This is a std::string";
    std::string CppStr3 = "This is an other std::string";

    std::string CppMultiLineString = R"(
        this is a muilti line screen,
        the \\n will automaticaly done.

        This can be pretty useful.
    )";

    std::cout << CppMultiLineString << std::endl;
    
    if(CppStr1.compare(CppStr2) == 0)
    {
        std::cout << "CppStr1 is equal to CppStr2 \n";
    }

    if(CppStr1.compare(CppStr3) == 0)
    {
        std::cout << "CppStr1 is equal to CppStr3 \n";
    } else
    {
        std::cout << "CppStr1 is not equal to CppStr3 \n";
    }

    int Size = CppStr1.size();
    char CharacterAt = CppStr1.at(3);

    if(CppStr1.find("std::string") != std::string::npos)
    {
        std::cout << "CppStr1 contains std::string \n";
    }

    if(CppStr1.find("c_str") != std::string::npos)
    {
        std::cout << "CppStr1 contains c_str \n";
    } else 
    {
        std::cout << "CppStr1 does not contains c_str \n";
    }

    const char* c_str = CppStr1.c_str();
}
#include "Bitfield.h"
#include <iostream>
#include <bitset>

typedef unsigned char Byte;
union Bits
{
    Byte Value;
    struct
    {
        Byte _1 : 1; // assign 1 bit to _1
        Byte _2 : 1;
        Byte _3 : 1;
        Byte _4 : 1;
        Byte _5 : 1;
        Byte _6 : 1;
        Byte _7 : 2; // assign two bit to _7
    };
};

void BitField()
{
    Bits bits;

    bits._1 = 1;
    bits._5 = 1;
    bits._7 = 1;

    std::cout << (unsigned int)bits.Value << std::endl;

    std::bitset<6> Values;
    Values[0] = 1;
    Values[4] = 1;
    Values[6] = 1;

    std::cout << Values << std::endl;
    std::cout << Values.to_ulong() << std::endl;

    std::cout << sizeof(bits) << std::endl;
    std::cout << sizeof(Values) << std::endl;
}
#include "Speed.h"
#include "Debuging/Timing/Timer.h"
#include <list>
#include <deque>
#include <vector>

void ExecSpeedTest()
{
    std::list<int> IntList;
    std::vector<int> IntVec;
    std::deque<int> IntDeque;

    long result = 0;
    
    {
        Timer T("List writing");
        for(int i = 0; i < 1000000; i++)
        {
            IntList.emplace_back(i);
        }
    }

    {
        Timer T("Vector writing");
        for(int i = 0; i < 1000000; i++)
        {
            IntVec.emplace_back(i);
        }
    }

    {
        Timer T("Deque writing");
        for(int i = 0; i < 1000000; i++)
        {
            IntDeque.emplace_back(i);
        }
    }
    std::cout << std::endl;
    {
        Timer T("List reading");

        for(int elem: IntList)
        {
            result += elem;
        }
    }
    result = 0;

    {
        Timer T("Vector reading");

        for(int elem: IntVec)
        {
            result += elem;
        }
    }
    result = 0;

    {
        Timer T("Deque reading");

        for(int elem: IntDeque)
        {
            result += elem;
        }
    }
}
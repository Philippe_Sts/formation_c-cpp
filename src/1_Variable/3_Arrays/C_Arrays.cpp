#include "C_Arrays.h"

#include <iostream>

void C_Arrays()
{
    int IntArray[5] = { 5, 4, 3, 2, 1 };

    IntArray[0] = 5;

    //This is what the [] operator does.
    *(IntArray + 0) = 5;

    int Val = IntArray[0];

    //int Val = *(IntArray + 0);

    for(size_t i = 0; i < 5; i++)
    {
        std::cout << "Array at " << i + 1 << " is equal to: " << IntArray[i] << std::endl;
    }

    for(size_t i = 0; i < 5; i++)
    {
        std::cout << "Array at " << i + 1 << " is equal to: " << *(IntArray + i) << std::endl;
    }

    // Size_t
    /*
    for(size_t i = 4; i >= 0; i--)
    {
        std::cout << "Array at " << i + 1 << " is equal to: " << IntArray[i] << std::endl;
    }
    */
}
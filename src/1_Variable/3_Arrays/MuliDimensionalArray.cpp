#include "MultiDimensionalArray.h"

#include "Debuging/Timing/Timer.h"
#include <array>

void MultiDimensionalArray()
{
    int Mat4[4][4] = 
    {
        { 1, 0, 0, 0 },
        { 0, 1, 0, 0 },
        { 0, 0, 1, 0 },
        { 0, 0, 0, 1 }
    };

    //!!! Require one extra {} !!!!
    std::array<std::array<int, 4>, 4> CppMat4 = 
    {{
        { 1, 0, 0, 0 },
        { 0, 1, 0, 0 },
        { 0, 0, 1, 0 },
        { 0, 0, 0, 1 }
    }};
}

void NonOptimizeLargeArray()
{
    //If the variable or array is too large for the stack:

    //int = 4 byte
    //4byte * 5000 * 5000 = 100MB
    //Maximum stack size is 8MB for moder linux OS and 1MB for windows

    //THIS WILL BE EXTREMELY SLOW!!!!
    Timer T("Non optimize large multidimentional array: ");
    //C Syntax
    /*
    int** Mat5000 = (int**)malloc(sizeof(int*) * 5000);
    for(size_t i = 0; i < 5000; i++)
    {
        Mat5000[i] = (int*)malloc(sizeof(int) * 5000);
    }
    */

    int** Mat5000 = new int*[5000];
    for(size_t i = 0; i < 5000; i++)
    {
        Mat5000[i] = new int[5000];
    }

    for(size_t i = 0; i < 5000; i++)
    {
        for(size_t j = 0; j < 5000; j++)
        {
            Mat5000[i][j] = 1;
        }
    }
    for (size_t i = 0; i < 5000; i++)
    {
        delete[] Mat5000[i];
    }
    delete[] Mat5000;
}

void OptimizeLargeArray()
{
    //If the variable or array is too large for the stack:

    //int = 4 byte
    //4byte * 5000 * 5000 = 100MB
    //Maximum stack size is 8MB for moder linux OS and 1MB for windows

    Timer T("Optimize large multidimentional array: ");
    //C Syntax
    /*
    int* Mat5000 = (int*)malloc(sizeof(int) * 5000 * 5000);
    */
    
    int* Mat5000 = new int[5000 * 5000];
    
    for(size_t i = 0; i < 5000; i++)
    {
        for(size_t j = 0; j < 5000; j++)
        {
            Mat5000[i + j * 5000] = 1;
        }
    }
    delete[] Mat5000;
}

void MultiDimensionalArrayOptimization()
{
    NonOptimizeLargeArray();
    OptimizeLargeArray();
}
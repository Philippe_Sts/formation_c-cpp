#include "CppArrays.h"
#include "Debuging/ObjectWithCopyMove/ObjCpyMvCnstr.h"
#include <iostream>
#include <array>
#include <vector>
#include <memory>

void CppArray()
{
    std::array<int, 5> CppArray = { 1, 2, 3, 4, 5 };

    for(size_t i = 0; i < CppArray.size(); i++)
    {
        std::cout << "Array at " << i << " is equal to: " << CppArray[i] << std::endl;
    }

    //Dynamic array
    std::vector<int> CppVector;
    CppVector.push_back(5);
    CppVector.push_back(4);
    CppVector.push_back(3);
    CppVector.push_back(2);
    CppVector.push_back(1);

    for(size_t i = 0; i < CppVector.size(); i++)
    {
        std::cout << "Vector at " << i << " is equal to: " << CppVector[i] << std::endl;
    }

    std::cout << "\nPop back element.\n";
    CppVector.pop_back();
    for(size_t i = 0; i < CppVector.size(); i++)
    {
        std::cout << "Vector at " << i << " is equal to: " << CppVector[i] << std::endl;
    }

    std::cout << "\nErase one element.\n";
    CppVector.erase(CppVector.begin() + 2);
    for(size_t i = 0; i < CppVector.size(); i++)
    {
        std::cout << "Vector at " << i<< " is equal to: " << CppVector[i] << std::endl;
    }

    std::cout << "\nPush back two elements.\n";
    CppVector.push_back(3);
    CppVector.push_back(1);
    for(size_t i = 0; i < CppVector.size(); i++)
    {
        std::cout << "Vector at " << i<< " is equal to: " << CppVector[i] << std::endl;
    }

    std::cout << "\nErase element at index 1 & 2\n";
    CppVector.erase(CppVector.begin() + 1, CppVector.begin() + 3);
    for(size_t i = 0; i < CppVector.size(); i++)
    {
        std::cout << "Vector at " << i << " is equal to: " << CppVector[i] << std::endl;
    }

}

int CreatedObject = 0;
int DestroyObject = 0;
int CopyDone = 0;
int MoveDone = 0;

struct VecObject
{
    //VecObject() = delete;
    VecObject()
    {
        //std::cout << "OBJECT CREATED" << std::endl;
        CreatedObject++;
    }

    VecObject(int inVal)
        : val(inVal)
    {
        //std::cout << "OBJECT CREATED" << std::endl;
        CreatedObject++;
    }

    VecObject(const VecObject& inVal)
    {
        val = inVal.val;
        //std::cout << "COPY DONE" << std::endl;
        CopyDone++;
    }

    VecObject(VecObject&& inVal)
    {
        val = inVal.val;
        //std::cout << "MOVE DONE" << std::endl;
        MoveDone++;
    }

    ~VecObject()
    {
        //std::cout << "OBJECT DESTROY" << std::endl;
        DestroyObject++;
    }

    int val;
};

void CppVectorOptimization()
{
    //emplace_back() or push_back()
    std::cout << "Non optimize Vector with push_back: " << std::endl;

    std::vector<TestObject> CppVector;
    CppVector.push_back(5);
    CppVector.push_back(5);
    CppVector.push_back(5);

    TestObject::PrintObjectStat();
    TestObject::ResetObjectStat();

    std::cout << "\nNon optimize Vector with emplace_back: " << std::endl;
    
    std::vector<TestObject> CppEmplaceVector;
    CppEmplaceVector.emplace_back(5);
    CppEmplaceVector.emplace_back(5);
    CppEmplaceVector.emplace_back(5);

    TestObject::PrintObjectStat();
    TestObject::ResetObjectStat();

    std::cout << "\nNot optimize Vector with push_back : " << std::endl;
    std::vector<TestObject> CppOptiVector;
    CppOptiVector.push_back(5);
    CppOptiVector.push_back(4);
    CppOptiVector.push_back(3);
    CppOptiVector.push_back(2);
    CppOptiVector.push_back(1);

    TestObject::PrintObjectStat();
    TestObject::ResetObjectStat();

    std::cout << "\nOptimize Vector with push_back : " << std::endl;
    std::vector<TestObject> CppOptiVectorPush;
    CppOptiVectorPush.reserve(5);
    CppOptiVectorPush.push_back(5);
    CppOptiVectorPush.push_back(4);
    CppOptiVectorPush.push_back(3);
    CppOptiVectorPush.push_back(2);
    CppOptiVectorPush.push_back(1);

    TestObject::PrintObjectStat();
    TestObject::ResetObjectStat();

    std::cout << "\nOptimize Vector with emplace_back : " << std::endl;
    std::vector<TestObject> CppOptiVector2;
    CppOptiVector2.reserve(5);
    CppOptiVector2.emplace_back(5);
    CppOptiVector2.emplace_back(4);
    CppOptiVector2.emplace_back(3);
    CppOptiVector2.emplace_back(2);
    CppOptiVector2.emplace_back(1);

    TestObject::PrintObjectStat();
    TestObject::ResetObjectStat();

    std::cout << "\nOptimize Vector with resize : " << std::endl;
    std::vector<TestObject> CppOptiVector3;
    CppOptiVector3.resize(5);
    CppOptiVector3[0].val = 5;
    CppOptiVector3[1].val = 4;
    CppOptiVector3[2].val = 3;
    CppOptiVector3[3].val = 2;
    CppOptiVector3[4].val = 1;

    TestObject::PrintObjectStat();
    TestObject::ResetObjectStat();

    std::cout << "This only work if you don't need to pass any args to the constructor.\n";
    

    std::cout << "\n";
}

void ProblemWithEmplaceBack()
{
    std::vector<std::unique_ptr<int>> Vect;

    int Val = 5;

    Vect.emplace_back(&Val);
    
    //Push back won't compile, while emplace_back will. and create bug
    //Vect.push_back(&Val);

    std::cout << "Value of Val is: " << *Vect[0] << std::endl;
    //This will throw an error:
    /*
    free(): invalid pointer
    Aborted (core dumped)

    OR

    munmap_chunk(): invalid pointer
    Aborted (core dumped)

    Here the unique_ptr is trying to free memory it does not own.
    */
}
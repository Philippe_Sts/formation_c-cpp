########################################################
#	PROJECT SETTINGS
########################################################
PROJECT_NAME 	= ExempleCpp
OUTPUT_NAME 	= ExempleCpp
SRC_BASE 		=	.
DEFINES 		= 

########################################################
#	DIRECTORIES
########################################################
BUILD_DIR = build/exec
TEMP_DIR = build/temp
SOURCES_DIR = src
INCLUDE_DIR = -I./src

########################################################
#	SOURCE FILES
########################################################
C_SRCS += $(wildcard $(SOURCES_DIR)/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*/*/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*/*/*/*.c)
C_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*/*/*/*/*.c)

CPP_SRCS += $(wildcard $(SOURCES_DIR)/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*/*/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*/*/*/*.cpp)
CPP_SRCS += $(wildcard $(SOURCES_DIR)/*/*/*/*/*/*/*/*/*/*.cpp)

COBJECTS64		:= $(patsubst %.c, $(TEMP_DIR)/obj64/%.o, $(C_SRCS))
CXXOBJECTS64	:= $(patsubst %.cpp, $(TEMP_DIR)/obj64/%.o, $(CPP_SRCS))
ALL_OBJECTS64	:= $(sort $(COBJECTS64) $(CXXOBJECTS64))

########################################################
#	COMPILER
########################################################
CC= ccache gcc
CPP= ccache g++

########################################################
#	COMPILER OPTIONS
########################################################
debug: OPTIMIZATION = -g3
release: OPTIMIZATION = -O3

########################################################
#	COMPILER FLAGS
########################################################
CFLAGS = $(DEFINES) $(INCLUDE_DIR) -fPIC -fvisibility=hidden
CPPFLAGS = $(DEFINES) $(INCLUDE_DIR) -fPIC -fvisibility=hidden -std=c++17

########################################################
#	LINKER
########################################################
LD= g++

########################################################
#	LINKER OPTIONS
########################################################
debug: LD_OPTIMIZATION = 
release: LD_OPTIMIZATION = -flto

########################################################
#	LINKER FLAGS
########################################################
LDFLAGS = -o $(TARGET) $(ALL_OBJECTS64)

########################################################
#	GENERATE OBJECT FILES
########################################################
OBJECTS = $(C_SRCS:.c=.o) $(CPP_SRCS:.cpp=.o)

########################################################
#	TOOLS
########################################################
debug: OUTPUT = $(BUILD_DIR)/lin_x64/D_Example
release: OUTPUT = $(BUILD_DIR)/lin_x64/R_Example

REMOVE = rm -rf

# Phony directive tells make that these are "virtual" targets, even if a file named "clean" exists.
.PHONY: all clean $(OUTPUT_NAME)
# Secondary tells make that the .o files are to be kept - they are secondary derivatives, not just
# temporary build products.
.SECONDARY: $(ALL_OBJECTS64)

debug release: $(OBJECTS)
	@echo Linking $(OUTPUT)
	mkdir -p $(dir $(OUTPUT))
	cp -Rf Assets/ build/exec/lin_x64/
	$(LD) $(LD_OPTIMIZATION) -flto -pthread -o $(OUTPUT) $(ALL_OBJECTS64) $(LIBS)

########################################################
#	GENERAL COMPILATION RULES
########################################################
.c.o :
	mkdir -p $(TEMP_DIR)/obj64/$(dir $<)
	$(CC) $(CFLAGS) $(OPTIMIZATION) -c $< -o $(TEMP_DIR)/obj64/$(<:.c=.o)
	
.cpp.o :
	mkdir -p $(TEMP_DIR)/obj64/$(dir $<)
	$(CPP) $(CPPFLAGS) $(OPTIMIZATION) -c $< -o $(TEMP_DIR)/obj64/$(<:.cpp=.o)

########################################################
#	CLEAN
########################################################
clean:
	$(REMOVE) build/
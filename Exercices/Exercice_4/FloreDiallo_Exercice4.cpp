#include "DRef.h"
#include <unordered_map>
#include <string>

class InternalDRef : public iDRef
{
public:
    InternalDRef()
    {
    }

    ~InternalDRef()
    {
    }
    void SetType(DRefTypes type)
    {
        m_Type = type;
    }

    DRefTypes GetType()
    {
        return m_Type;
    };

    int GetIntValue()
    {
        return m_ArrayVal[0];
    }

    void SetIntValue(int value)
    {
        m_ArrayVal[0] = value;
    }

    int GetArrayValueAt(size_t index)
    {
        if (index < m_ArrayVal.size())
            return m_ArrayVal[index];
        return 0;
    }

    void SetArrayValueAt(size_t index, int value)
    {
        if (index >= m_ArrayVal.size())
        {
            ResizeArray(index + 1);
        }
        // if (index < m_ArrayVal.size())
        // {
        m_ArrayVal[index] = value;
        // }
    }

    void ResizeArray(size_t size)
    {
        m_ArrayVal.resize(size);
    }

private:
    std::vector<int> m_ArrayVal;
};

namespace
{
    std::unordered_map<std::string, InternalDRef *> DrefMap;
}

iDRef *CreateDRef(const char *name, DRefTypes Type, size_t size)
{
    switch (Type)
    {
    case DRefTypes::TYPE_INT:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->ResizeArray(1);
        DrefMap[name]->SetIntValue(0);
        return DrefMap[name];
        break;

    default:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->ResizeArray(size);
        return DrefMap[name];
        break;
    }
}

iDRef *FindDRef(const char *name)
{
    auto it = DrefMap.find(name);
    if (it != DrefMap.end())
        return DrefMap[name];
    return nullptr;
}

void SetIntDRef(iDRef *dref, int value)
{
    if (dref)
    {
        dynamic_cast<InternalDRef *>(dref)->SetIntValue(value);
    }
}

void SetArrayAtDref(iDRef *dref, size_t index, int value)
{
    if (dref)
    {
        dynamic_cast<InternalDRef *>(dref)->SetArrayValueAt(index, value);
    }
}

int GetIntDRef(iDRef *dref)
{
    if (dref)
    {
        return (dynamic_cast<InternalDRef *>(dref)->GetIntValue());
    }
    return 0;
}

int GetArrayAtDref(iDRef *dref, size_t index)
{
    if (dref)
    {
        return (dynamic_cast<InternalDRef *>(dref)->GetArrayValueAt(index));
    }
    return 0;
}

#include <iostream>
#include "DRef.h"

class DatarefArrayElement
{
public:
    DatarefArrayElement(iDRef* dref, size_t index)
        : m_Dref(dref), m_Index(index)
    {
        m_Value = GetArrayAtDref(m_Dref, m_Index);
    }

    ~DatarefArrayElement()
    {

    }

    DatarefArrayElement& operator= (const DatarefArrayElement& val)
    {
        SetArrayAtDref(m_Dref, m_Index, val.m_Value);
        return *this;
    }

    DatarefArrayElement& operator= (float val)
    {
        SetArrayAtDref(m_Dref, m_Index, val);
        return *this;
    }

    DatarefArrayElement& operator= (int val)
    {
        SetArrayAtDref(m_Dref, m_Index, val);
        return *this;
    }

    DatarefArrayElement& operator= (double val)
    {
        SetArrayAtDref(m_Dref, m_Index, val);
        return *this;
    }

    explicit operator char()
    {
        return GetArrayAtDref(m_Dref, m_Index);
    }

    explicit operator int()
    {
        return GetArrayAtDref(m_Dref, m_Index);
    }

    explicit operator float()
    {
        return GetArrayAtDref(m_Dref, m_Index);
    }

    explicit operator double()
    {
        return GetArrayAtDref(m_Dref, m_Index);
    }

    friend std::ostream& operator <<(std::ostream& os, const DatarefArrayElement& dref)
    {
        os << "Value at index " << dref.m_Index << " is : " << dref.m_Value;
        return os;
    }

private:
    DatarefArrayElement() = delete;

    iDRef* m_Dref;
    size_t m_Index;
    int m_Value;
};

class Dataref
{
public:
    Dataref(const char* name, DRefTypes type, size_t size = 0)
    {
        m_Dref = CreateDRef(name, type, size);
    }

    ~Dataref()
    {

    }

    DatarefArrayElement operator[] (int index)
    {
        return DatarefArrayElement(m_Dref, index);
    }

    Dataref& operator= (const Dataref& val)
    {
        int value = GetIntDRef(val.m_Dref);
        SetIntDRef(m_Dref, value);
        return *this;
    }

    Dataref& operator= (float val)
    {
        SetIntDRef(m_Dref, val);
        return *this;
    }

    Dataref& operator= (int val)
    {
        SetIntDRef(m_Dref, val);
        return *this;
    }

    Dataref& operator= (double val)
    {
        SetIntDRef(m_Dref, val);
        return *this;
    }

    explicit operator char()
    {
        return GetIntDRef(m_Dref);
    }

    explicit operator int()
    {
        return GetIntDRef(m_Dref);
    }

    explicit operator float()
    {
        return GetIntDRef(m_Dref);
    }

    explicit operator double()
    {
        return GetIntDRef(m_Dref);
    }

    friend std::ostream& operator <<(std::ostream& os, const Dataref& dref)
    {
        os << "Value is " << GetIntDRef(dref.m_Dref);
        return os;
    }

private:
    Dataref() = delete;

    iDRef* m_Dref;
};

int main(int argc, char** argv)
{    
    /*
    
    operator= Dataref int
    operator(int) ... 
    operator[]
    operator<< 

    */
    Dataref dref = Dataref("dref1", DRefTypes::TYPE_INT);
    dref = 7;

    std::cout << dref << std::endl;

    int i = (int)dref;

    std::cout << i << std::endl;

    Dataref arrayRef = Dataref("dref2", DRefTypes::TYPE_ARRAY, 3);

    arrayRef[0] = 5;

    std::cout << arrayRef[0] << std::endl;
}
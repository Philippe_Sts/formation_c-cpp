#include "DRef.h"
#include <unordered_map>
#include <string>
#include <memory> // pour le shared_ptr
#include <vector> // changer de place
#include <iostream>

class InternalDRef : public iDRef
{
public:
    InternalDRef()
    {
    }

    ~InternalDRef()
    {

    }

    int GetIntValue() const
    {
        if(m_Type == DRefTypes::TYPE_INT)
        {
            return m_ArrayVal[0];
        }
        return 0;
    }

    void SetIntValue(int value)
    {
        if(m_Type == DRefTypes::TYPE_INT)
        {
            m_ArrayVal[0] = value;
        }
    }

    int GetArrayValueAt(size_t index) const
    {
        if(m_Type == DRefTypes::TYPE_ARRAY && index < m_ArrayVal.size())
        {
            return m_ArrayVal[index];
        }
        return 0;
    }

    void SetArrayValueAt(size_t index, int value)
    {
        if(m_Type == DRefTypes::TYPE_ARRAY && index < m_ArrayVal.size())
        {
            m_ArrayVal[index] = value;
        }
    }

    void ResizeArray(size_t size)
    {
        m_ArrayVal.resize(size);
    }

    // Ajouter
    void SetType(DRefTypes type)
    {
        m_Type = type;
    }
    
    // si besoin.
    DRefTypes GetType() const
    {
        return m_Type;
    }

private:
    std::vector<int> m_ArrayVal;
};

namespace
{
    //Utiliser un shared_ptr
    std::unordered_map<std::string, std::shared_ptr<InternalDRef>> DrefMap;
}

iDRef* CreateDRef(const char* name, DRefTypes Type, size_t size)
{
    if(FindDRef(name))
    {
        return DrefMap[name].get();
    }
    DrefMap[name] = std::make_shared<InternalDRef>();
    switch (Type)
    {
    case DRefTypes::TYPE_INT:
        DrefMap[name]->SetType(Type);
        DrefMap[name]->ResizeArray(1);
        DrefMap[name]->SetIntValue(0);
        return DrefMap[name].get();
    
    //pas default mais case ... :
    case DRefTypes::TYPE_ARRAY:
        DrefMap[name]->SetType(Type);
        DrefMap[name]->ResizeArray(size);
        return DrefMap[name].get();
    }
    return nullptr;
}

iDRef* FindDRef(const char* name)
{
    return DrefMap[name].get();
}

void SetIntDRef(iDRef* dref, int value)
{
    //dynamic cast.
    InternalDRef* InDref = dynamic_cast<InternalDRef*>(dref);
    InDref->SetIntValue(value);
}

void SetArrayAtDref(iDRef* dref, size_t index, int value)
{
    InternalDRef* InDref = dynamic_cast<InternalDRef*>(dref);
    InDref->SetArrayValueAt(index, value);
}

int GetIntDRef(iDRef* dref)
{
    InternalDRef* InDref = dynamic_cast<InternalDRef*>(dref);
    return InDref->GetIntValue();
}

int GetArrayAtDref(iDRef* dref, size_t index)
{
    InternalDRef* InDref = dynamic_cast<InternalDRef*>(dref);
    return InDref->GetArrayValueAt(index);
}

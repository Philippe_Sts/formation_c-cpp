#include "DRef.h"
#include <unordered_map>
#include <string>
#include <memory>

class InternalDRef : public iDRef
{
public:
    InternalDRef()
    {

    }

    ~InternalDRef()
    {

    }

    int GetIntValue()
    {

    }

    void SetIntValue(int value)
    {
        m_ArrayVal[0] = value;
    }

    int GetArrayValueAt(size_t index)
    {

    }

    int SetArrayValueAt(size_t index, int value)
    {

    }

    void ResizeArray(size_t size)
    {
        m_ArrayVal.resize(size);
    }

private:
    std::vector<int> m_ArrayVal;
};

namespace
{
    std::unordered_map<std::string, std::shared_ptr<InternalDRef>> DrefMap;
}

iDRef* CreateDRef(const char* name, DRefTypes Type, size_t size = 0)
{
    DrefMap[name] = std::make_shared<InternalDRef>();
    switch (Type)
    {
    case DRefTypes::TYPE_INT:
        DrefMap[name]->SetType(Type);
        DrefMap[name]->ResizeArray(1);
        DrefMap[name]->SetIntValue(0);
        return DrefMap[name].get();
        break;
    
    default:
        DrefMap[name]->SetType(Type);
        DrefMap[name]->ResizeArray(size);
        return DrefMap[name].get();
        break;
    }
}

iDRef* FindDRef(const char* name)
{
    
}

void SetIntDRef(iDRef* dref, int value)
{
    
}

void SetArrayAtDref(iDRef* dref, size_t index, int value)
{

}

int GetIntDRef(iDRef* dref)
{

}

int GetArrayAtDref(iDRef* dref, size_t index)
{

}

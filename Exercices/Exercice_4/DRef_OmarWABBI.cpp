#include "DRef.h"
#include <unordered_map>
#include <string>

class InternalDRef : public iDRef
{
public:
    InternalDRef() 
    {

    }

    ~InternalDRef()
    {

    }

    int GetIntValue()
    {
        return m_ArrayVal[0];
    }

    void SetIntValue(int value)
    {
        m_ArrayVal[0] = value;
    }

    int GetArrayValueAt(size_t index)
    {
        return m_ArrayVal[index];
    }

    void SetArrayValueAt(size_t index, int value)
    {
        m_ArrayVal[index] = value;
    }

    void ResizeArray(size_t size)
    {
        m_ArrayVal.resize(size);
    }

private:
    std::vector<int> m_ArrayVal;
};

namespace
{
    std::unordered_map<std::string, InternalDRef*> DrefMap;
}

iDRef* CreateDRef(const char* name, DRefTypes Type, size_t size = 0)
{
    switch (Type)
    {
    case DRefTypes::TYPE_INT:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->ResizeArray(1);
        DrefMap[name]->SetIntValue(0);
        return DrefMap[name];
        break;
    
    default:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->ResizeArray(size);
        return DrefMap[name];
        break;
    }
}

iDRef* FindDRef(const char* name)
{
    return DrefMap[name];
}

void SetIntDRef(iDRef* dref, int value)
{
    InternalDRef * IDref = dynamic_cast<InternalDRef *>(dref);
    IDref->SetIntValue(value);
}

void SetArrayAtDref(iDRef* dref, size_t index, int value)
{
    InternalDRef * IDref = dynamic_cast<InternalDRef *>(dref);
    IDref->SetArrayValueAt(index, value);
}

int GetIntDRef(iDRef* dref)
{
    InternalDRef * IDref = dynamic_cast<InternalDRef *>(dref);
    return IDref->GetIntValue();
}

int GetArrayAtDref(iDRef* dref, size_t index)
{
    InternalDRef * IDref = dynamic_cast<InternalDRef *>(dref);
    return IDref->GetArrayValueAt(index);
}

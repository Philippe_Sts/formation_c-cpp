#include "DRef.h"
#include <unordered_map>
#include <string>

class InternalDRef : public iDRef
{
public:
    InternalDRef()
    {

    }

    ~InternalDRef()
    {

    }

    int GetIntValue()
    {
        return m_ArrayVal[0];
    }

    void SetIntValue(int value)
    {
        m_ArrayVal[0] = value;
    }

    int GetArrayValueAt(size_t index)
    {       
       if (m_Type == DRefTypes::TYPE_ARRAY) {            
            if (index < m_ArrayVal.size())
            {
                
                return m_ArrayVal[index];
            }   else
            {
                return -1;
            }
       } else
       {
            return -1;
       }
    }

    void SetArrayValueAt(size_t index, int value)
    {
        
       if (m_Type == DRefTypes::TYPE_ARRAY) {
            if (index < m_ArrayVal.size())
            {
                m_ArrayVal[index] = value;
            }   else
            {
                m_ArrayVal[0] = value;
            }
       } else
       {
            m_ArrayVal[0] = value;
       }
    }

    void ResizeArray(size_t size)
    {
        m_ArrayVal.resize(size);
        if (size > 0) {
            m_Type = DRefTypes::TYPE_ARRAY;
        } else
        {
            m_Type = DRefTypes::TYPE_INT;
        }
    }

private:
    std::vector<int> m_ArrayVal;
};

namespace
{
    std::unordered_map<std::string, InternalDRef*> DrefMap;
}

iDRef* CreateDRef(const char* name, DRefTypes Type, size_t size)
{
    switch (Type)
    {
    case DRefTypes::TYPE_INT:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->ResizeArray(1);
        DrefMap[name]->SetIntValue(0); 
        return DrefMap[name];
        break;
    
    default:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->ResizeArray(size);
        return DrefMap[name];
        break;
    }
}

iDRef* FindDRef(const char* name)
{
    return DrefMap[name] ;
}

void SetIntDRef(iDRef* dref, int value)
{
    (dynamic_cast<InternalDRef*>(dref))->SetIntValue(value);   
}

void SetArrayAtDref(iDRef* dref, size_t index, int value)
{  
    (dynamic_cast<InternalDRef*>(dref))->SetArrayValueAt(index,value);   
}

int GetIntDRef(iDRef* dref)
{
    return (dynamic_cast<InternalDRef*>(dref))->GetIntValue();
}

int GetArrayAtDref(iDRef* dref, size_t index)
{
    return (dynamic_cast<InternalDRef*>(dref))->GetArrayValueAt(index);
}

#include "DRef.h"
#include <unordered_map>
#include <string>
#include <iostream>

using namespace std;

class InternalDRef : public iDRef
{
public:
    InternalDRef()
    {

    }

    ~InternalDRef()
    {

    }

    int GetIntValue()
    {
        if (m_Type == DRefTypes::TYPE_INT)
            return m_ArrayVal[0];
        cout << "Can't do" << endl;
        return -666;
    }

    void SetIntValue(int value)
    {
        m_ArrayVal[0] = value;
    }

    int GetArrayValueAt(size_t index)
    {
        if (m_Type == DRefTypes::TYPE_ARRAY && index < m_ArrayVal.size())
            return m_ArrayVal[index];
        return -666;
    }

    void SetArrayValueAt(size_t index, int value)
    {
        if (m_Type == DRefTypes::TYPE_ARRAY && index < m_ArrayVal.size())
            m_ArrayVal[index] = value;
        else
            cout << "Can't do" << endl;
    }

    void ResizeArray(size_t size)
    {
        m_ArrayVal.resize(size);
    }

private:
    std::vector<int> m_ArrayVal;
};

namespace
{
    std::unordered_map<std::string, InternalDRef*> DrefMap;
}

iDRef* CreateDRef(const char* name, DRefTypes Type, size_t size)
{
    switch (Type)
    {
    case DRefTypes::TYPE_INT:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->ResizeArray(1);
        DrefMap[name]->SetIntValue(0);
        return DrefMap[name];
        break;
    
    default:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->ResizeArray(size);
        return DrefMap[name];
        break;
    }
}

iDRef* FindDRef(const char* name)
{
    auto it = DrefMap.find(name);
    if (it == DrefMap.end())
        return nullptr;
    else
        return it->second;
}

void SetIntDRef(iDRef* dref, int value)
{
    dynamic_cast<InternalDRef*>(dref)->SetIntValue(value);
}

void SetArrayAtDref(iDRef* dref, size_t index, int value)
{
    dynamic_cast<InternalDRef*>(dref)->SetArrayValueAt(index, value);
}

int GetIntDRef(iDRef* dref)
{
    return dynamic_cast<InternalDRef*>(dref)->GetIntValue();
}

int GetArrayAtDref(iDRef* dref, size_t index)
{
    return dynamic_cast<InternalDRef*>(dref)->GetArrayValueAt(index);
}

int main(int argc, char** argv)
{
    cout << "hello" << endl;
    return 0;
}
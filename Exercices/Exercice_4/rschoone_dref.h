#pragma once
#include <vector>

enum class DRefTypes
{
    TYPE_INT,
    TYPE_ARRAY
};

class iDRef
{
public:
    virtual ~iDRef(){}

protected:
    DRefTypes m_Type;
};

iDRef* CreateDRef(const char* name, DRefTypes Type, size_t size = 0);
iDRef* FindDRef(const char* name);

void SetIntDRef(iDRef* dref, int value);
void SetArrayAtDref(iDRef* dref, size_t index, int value);
int GetIntDRef(iDRef* dref);
int GetArrayAtDref(iDRef* dref, size_t index);
#include "DRef.h"
#include <unordered_map>
#include <string>

class InternalDRef : public iDRef
{
public:
    InternalDRef()
    {

    }

    ~InternalDRef()
    {

    }

    int GetIntValue()
    {
        return m_ArrayVal[0];
    }

    void SetIntValue(int value)
    {
        m_ArrayVal[0] = value;
    }

    int GetArrayValueAt(size_t index)
    {
        return m_ArrayVal[index];
    }

    int SetArrayValueAt(size_t index, int value)
    {
        m_ArrayVal[index] = value;
    }

    void ResizeArray(size_t size)
    {
        m_ArrayVal.resize(size);
    }

    void SetDRefTypes(DRefTypes Type)
    {
        m_Type = Type;
    }

    DRefTypes GetDRefTypes()
    {
        return m_Type;
    }


private:
    std::vector<int> m_ArrayVal;
};

namespace
{
    std::unordered_map<std::string, InternalDRef*> DrefMap;
}

iDRef* CreateDRef(const char* name, DRefTypes Type, size_t size)
{
    switch (Type)
    {
    case DRefTypes::TYPE_INT:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->SetDRefTypes(DRefTypes::TYPE_INT);
        DrefMap[name]->ResizeArray(1);
        DrefMap[name]->SetIntValue(0);
        return DrefMap[name];
        break;
    
    default:
        DrefMap[name] = new InternalDRef();
        DrefMap[name]->SetDRefTypes(DRefTypes::TYPE_ARRAY);
        DrefMap[name]->ResizeArray(size);
        return DrefMap[name];
        break;
    }
}

iDRef* FindDRef(const char* name)
{
    return DrefMap.find(name)->second;
}

void SetIntDRef(iDRef* dref, int value)
{
    InternalDRef* ADref = dynamic_cast<InternalDRef*> (dref);
    ADref->SetIntValue(value);
}

void SetArrayAtDref(iDRef* dref, size_t index, int value)
{
    InternalDRef* ADref = dynamic_cast<InternalDRef*> (dref);
    ADref->SetArrayValueAt(index,value);
}

int GetIntDRef(iDRef* dref)
{
    InternalDRef* ADref = dynamic_cast<InternalDRef*> (dref);
    return ADref->GetIntValue();
}

int GetArrayAtDref(iDRef* dref, size_t index)
{
    InternalDRef* ADref = dynamic_cast<InternalDRef*> (dref);
    return ADref->GetArrayValueAt(index);
}

#include <iostream>
#include "DRef.h"

class Dataref
{
public:
    Dataref(const char* name, DRefTypes Type, size_t size = 0)
    {
        size_t ASize = size;
        ADref = CreateDRef(name, Type, ASize);
    }
    ~Dataref()
    {
    
    }

    int operator= (const Dataref& Dref)
    {
        return GetIntDRef(Dref.ADref);
        //return 0;
    }
    Dataref& operator= (const int val)
    {
        SetIntDRef(ADref,val);
        return *this;
    }

    int& operator[] (int const index)
    {
        AInt = GetArrayAtDref(ADref, index);
        std::cout << "Int Value : " << AInt << std::endl;
        return AInt;
    }   
    
    friend std::ostream& operator <<(std::ostream& os, const Dataref& Dref)
    {
        os << GetIntDRef(Dref.ADref) << " ";
        return os;
    }
    explicit operator unsigned int()
    {
        return Byte;
    }
private:
    iDRef* ADref;
    int AInt;
protected:
    unsigned int Byte;
};

int main(int argc, char** argv)
{
    std::cout << "Start Main ..." << std::endl;

   /* iDRef* AIntDref = CreateDRef("Value1", DRefTypes::TYPE_INT);
    iDRef* AnArrayDref = CreateDRef("ValueX", DRefTypes::TYPE_ARRAY,5);

    SetIntDRef(AIntDref,1);
    std::cout << "Int Value : " << GetIntDRef(AIntDref) << std::endl;

    SetArrayAtDref(AnArrayDref, 0, 5);
    std::cout << "Array Value : " << GetArrayAtDref(AnArrayDref,0) << std::endl;*/

    Dataref DataInt = Dataref("Value1", DRefTypes::TYPE_INT);
    DataInt = 5;
    std::cout << "Int Value : " << DataInt << std::endl;

    //int i = (int)DataInt;

    Dataref DataArray = Dataref("ValueX", DRefTypes::TYPE_ARRAY,5);

    DataArray[0] = 5;

    std::cout << "Array Value : "  << (int)DataArray[0] << std::endl;


    std::cout << "Stop Main ..." << std::endl;
}
#include <iostream>
#include "DRef.h"

int main(int argc, char** argv)
{
    iDRef* dref1 = CreateDRef("dref1", DRefTypes::TYPE_INT);
    iDRef* dref2 = CreateDRef("dref2", DRefTypes::TYPE_ARRAY, 3);
    
    SetIntDRef(dref1, 5);
    for(size_t i = 0; i < 3; i++)
    {
        SetArrayAtDref(dref2, i, i);
    }

    std::cout << GetArrayAtDref(dref2, 1) << std::endl;

}
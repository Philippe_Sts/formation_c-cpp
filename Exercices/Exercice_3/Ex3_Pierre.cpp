#include "TemplateClass.h"
#include <iostream>



int main(int argc, char** argv)
 {
     CustomArray<int> Arr(5);
     Arr[0] = 1;     
     Arr[1] = 32;     
     Arr[2] = 18;     
     Arr[3] = 5;
     std::cout << "\nArr[0] = " << Arr[0] << "\nSize = " << Arr.Size() << "\nAt(2) = " <<  Arr.At(2) << std::endl;
 }
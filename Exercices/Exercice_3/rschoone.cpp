#pragma once
#include <utility>
#include <cstddef>
#include <array>
#include <iostream>

namespace Templates
{
    template<class T, int N>
    class CustomArray
    {
    public:
        CustomArray();

        ~CustomArray();

        //void push_back(T value);

        //void pop_back(T value);

        //T& operator[] (int index);

        T& At(int Location);

        size_t size();

    private:
        T* m_Data;
        size_t m_Size;
    };

    template<typename T, int N>
    inline CustomArray<T,N>::CustomArray()
    {
        m_Size = N;
        m_Data = new T[m_Size];
    }

    template<typename T, int N>
    inline CustomArray<T,N>::~CustomArray()
    {
        delete[] m_Data;
    }

    template<typename T, int N>
    T& CustomArray<T,N>::At(int Location)
    {
        if(Location < m_Size)
        {
            return m_Data[Location];
        }
        else
        {
            std::cout << "Location is out of size" << std::endl;
        }
    }

    template<typename T, int N>
    inline size_t CustomArray<T, N>::size()
    {
        return m_Size;
    }
/*
    template<typename T>
    T& CustomArray<T>::operator[] (int index)
    {
        return m_Data[index];
    }*/

    void TemplateClassExample();
}


int main(int argc, char** argv)
{
        Templates::CustomArray<int,5> Vec;
        Vec.At(0) = 5;
        Vec.At(1) = 2;
        std::cout << "Size of my array : " << Vec.size() << std::endl;
        std::cout << "First element of my array : " << Vec.At(0)<< std::endl;
        std::cout << "Youpi" << std::endl;
}
#pragma once
#include <utility>
#include <cstddef>

    template<class T>
    class CustomArray
    {
    public:
        CustomArray(std::size_t aSize);

        ~CustomArray();

        T& operator[] (int index);

        T& At (int index);

        std::size_t Size();

    private:
        T* m_Data;
        size_t m_Size;
    };

    template<typename T>
    inline CustomArray<T>::CustomArray(std::size_t aSize)
    {
        m_Size = aSize;
        m_Data = new T[m_Size];
    }

    template<typename T>
    inline CustomArray<T>::~CustomArray()
    {
        delete[] m_Data;
    }


    template<typename T>
    inline T& CustomArray<T>::At(int index)
    {
        return m_Data[index];
    }

    template<typename T>
    inline std::size_t CustomArray<T>::Size()
    {
        return m_Size;
    }



    template<typename T>
    T& CustomArray<T>::operator[] (int index)
    {
        return m_Data[index];
    }

    void TemplateClassExample();


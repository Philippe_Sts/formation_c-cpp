#include<vector>
#include<iostream>

template <class T>
class MyArray {
    private:
        T* values;
        size_t arrSize;

    public:
        void set( int index, T item) {
            values[index] = item;
        }

        T at(int index) {
            if (index >= 0 && index < size())
                return values[index];
            else
                return 666;
        }

        size_t size() {
            return arrSize;
            // return sizeof(values)/sizeof(T);
        };

        void sort() {
            std::cout << "nope" << std::endl;
        }

        MyArray(int size) {
            values = new T[size];
            arrSize = size;
            std::cout << "constructor " << size << std::endl;
            std::cout << "sizeof values = " << sizeof(*values) << " & size of T " << sizeof(values[0]) << std::endl;
        }

        ~MyArray() {
            delete [] values;
        }
};

int main(int argc, char** argv) {
    MyArray<int> arr (4);
    arr.set(0, 25);
    arr.set(1, 42);
    arr.set(2, 13);

    int i = 0;
    while (i < arr.size())
    {
        std::cout << "at " << i << " = " << arr.at(i) << std::endl;    
        i++;
    }
}
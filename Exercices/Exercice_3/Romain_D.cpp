#include <utility>
#include <cstddef>
#include <iostream>

    template<class T, size_t N>
    class CustomArray
    {
    public:
        CustomArray();

        ~CustomArray();

        T& operator[] (int index);

        size_t size();

        T& at (int index);

    private:
        T* m_Data;
        size_t m_Size;
    };

    template<typename T, size_t N>
    inline CustomArray<T, N>::CustomArray()
    {
        m_Size = N;
        m_Data = new T[m_Size];
    }

    template<typename T, size_t N>
    inline CustomArray<T, N>::~CustomArray()
    {
        delete[] m_Data;
    }

    template<typename T, size_t N>
    T& CustomArray<T, N>::operator[] (int index)
    {
        return m_Data[index];
    }

    template<typename T, size_t N>
    T& CustomArray<T, N>::at (int index)
    {
        return m_Data[index];
    } 

    template<typename T, size_t N>
    size_t CustomArray<T, N>::size()
    {
        return m_Size;
    }

    void TemplateClassExample();

int main()
{
    CustomArray<int,5> cust;
    std::cout << "size of array =  " << cust.size() << std::endl;
    cust.at(1) = 54;
    std::cout << "Value of element 1 =  " << cust[1] << std::endl;
    return 0;
}


#pragma once
#include <utility>
#include <cstddef>

    template<class T, std::size_t I>
    class CustomArray
    {
    public:
        //CustomArray(std::size_t aSize);
        CustomArray();

        ~CustomArray();

        T& operator[] (int index);

        T& At (int index);

        std::size_t Size();

    private:
        T* m_Data;
        size_t m_Size;
    };

//    template<typename T>
//    inline CustomArray<T>::CustomArray(std::size_t aSize)
//    {
//        m_Size = aSize;
//        m_Data = new T[m_Size];
//    }
    template<class T,  std::size_t I>
    inline CustomArray<T, I>::CustomArray()
    {
        m_Size = I;
        m_Data = new T[m_Size];
    }

    template<class T, std::size_t I>
    inline CustomArray<T, I>::~CustomArray()
    {
        delete[] m_Data;
    }


    template<class T, std::size_t I>
    inline T& CustomArray<T, I>::At(int index)
    {
        return m_Data[index];
    }

    template<class T, std::size_t I>
    inline std::size_t CustomArray<T, I>::Size()
    {
        return m_Size;
    }



    template<class T, std::size_t I>
    T& CustomArray<T, I>::operator[] (int index)
    {
        return m_Data[index];
    }

    void TemplateClassExample();


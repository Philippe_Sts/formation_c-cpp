#pragma once
#include <utility>
#include <cstddef>

namespace Templates
{
    template<class T>
    class CustomVector
    {
    public:
        CustomVector();

        ~CustomVector();

        std::size_t Size();

        T& At(int n);

        T& operator[] (int index);

    private:
        T* m_Data;
        size_t m_Size;
    };

    template<typename T>
    inline CustomVector<T>::CustomVector()
    {
        m_Size = 0;
        m_Data = new T[m_Size];
    }

    template<typename T>
    inline CustomVector<T>::~CustomVector()
    {
        delete[] m_Data;
    }

    template<typename T>
    inline std::size_t CustomVector<T>::Size()
    {
       return m_size;
    }

    template<typename T>
    inline T& CustomVector<T>::At(int index)
    {
        return m_Data[index];
    }

    template<typename T>
    T& CustomVector<T>::operator[] (int index)
    {
        return m_Data[index];
    }

    void TemplateClassExample();
}
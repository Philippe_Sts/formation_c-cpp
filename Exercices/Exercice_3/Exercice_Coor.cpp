#include <iostream>

template<class T, size_t NM>
class CustomArray
{
public:
    CustomArray(){}
    ~CustomArray(){}

    size_t size()
    {
        return NM;
    }

    T at(size_t index)
    {
        return m_Data[index];
    }

    T& operator[] (size_t index)
    {
        return m_Data[index];
    }

private:
    T m_Data[NM];
};

int main()
{
    CustomArray<int, 5> a = CustomArray<int, 5>();

    for(size_t i = 0; i < a.size(); i++)
    {
        a[i] = i;
    }

    std::cout << a.at(2) << std::endl;
    std::cout << a[3] << std::endl;
    std::cout << a.size() << std::endl;
}
#pragma once
#include <cstddef>

#pragma region version 2: template with 2 
template <typename T, int N>
class CustomArray
{
    public:
    CustomArray();
    ~CustomArray();
    const int Size();
    const T& At(int index);

    T& operator[](int index);

    private:
    T* m_Data;
    int m_Size;

};

template <typename T, int N>
inline T& CustomArray<T, N>::operator[](int index)
{
    return *(m_Data + index);
}

template<typename T, int N>
inline CustomArray<T, N>::CustomArray()
{
    m_Size = N;
    m_Data = new T[N];
}

template<typename T, int N>
inline CustomArray<T, N>::~CustomArray()
{
    delete[] m_Data;
}

template<typename T, int N>
inline const int CustomArray<T, N>::Size()
{
    return m_Size;
}

template <typename T, int N>
inline const T& CustomArray<T,N>::At(int index)
{
    return *(m_Data + index);
}
#pragma endregion


#pragma region version 1: template with 1
/*
template <class T>
class CustomArray
{
    public:
    CustomArray(size_t size);
    ~CustomArray();
    const size_t Size();
    const T& At(int index);

    T& operator[](int index);

    private:
    T* m_Data;
    size_t m_Size;
    // Delete empty ctor to insure a size is given
    CustomArray() = delete;
};

template <typename T>
inline T& CustomArray<T>::operator[](int index)
{
    return *(m_Data + index);
}

template<typename T>
inline CustomArray<T>::CustomArray(const size_t size)
{
    m_Size = size;
    m_Data = new T[size];
}

template<typename T>
inline CustomArray<T>::~CustomArray()
{
    delete[] m_Data;
}

template<typename T>
inline const size_t CustomArray<T>::Size()
{
    return m_Size;
}

template <typename T>
inline const T& CustomArray<T>::At(int index)
{
    return *(m_Data + index);
}
*/
#pragma endregion
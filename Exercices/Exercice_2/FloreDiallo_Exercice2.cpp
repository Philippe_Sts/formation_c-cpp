class Connection
{
public:
    virtual void StartConnection() = 0;
    virtual void StopConnection() = 0;

    const bool ConnectionStatus() const
    {
        return m_ConnectionEstablished;
    }

protected:
    bool m_ConnectionEstablished = false;
};

class TcpConnection: public Connection
{
    public:
    virtual void StartConnection() override{}
    virtual void StopConnection() override{}
};

int main(int argc, char** argv)
{
    auto tcpConnection = new TcpConnection();
}
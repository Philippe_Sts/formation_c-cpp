#include <iostream>

class Connection
{
public:
    virtual void StartConnection() = 0;
    virtual void StopConnection() = 0;

    const bool ConnectionStatus() const
    {
        return m_ConnectionEstablished;
    }

protected:
    bool m_ConnectionEstablished = false;
};

class TCPConnection : public Connection
{
public:
    virtual void StartConnection() override 
    { 
        m_ConnectionEstablished = true;
        std::cout<<"TCP connection started"<<std::endl;
    }

    virtual void StopConnection() override 
    {
        m_ConnectionEstablished = false;
        std::cout<<"TCP connection closed"<<std::endl;        
    }
};

int main(int argc, char** argv)
{
    TCPConnection MyConnection;

    MyConnection.StartConnection();

    std::cout<< "connection : " << MyConnection.ConnectionStatus() << std::endl;

    MyConnection.StopConnection();
    
}
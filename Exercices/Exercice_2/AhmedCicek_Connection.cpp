#include <iostream>

class Connection
{
public:
    virtual void StartConnection() = 0;
    virtual void StopConnection() = 0;

    const bool ConnectionStatus() const
    {
        return m_ConnectionEstablished;
    }

protected:
    bool m_ConnectionEstablished = false;
};

class USB : public Connection
{
    public:
    void StartConnection(){
	std::cout << "Start the connection ..." << std::endl;
    	m_ConnectionEstablished = true;
    }

    void StopConnection(){
	std::cout << "End the connection ..." << std::endl; 
	m_ConnectionEstablished = false;
    }
};

int main(int argc, char** argv)
{
    USB usb;

    usb.StartConnection();
    usb.StopConnection();
}
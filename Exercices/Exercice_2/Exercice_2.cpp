class Connection
{
public:
    virtual void StartConnection() = 0;
    virtual void StopConnection() = 0;

    const bool ConnectionStatus() const
    {
        return m_ConnectionEstablished;
    }

protected:
    bool m_ConnectionEstablished = false;
};

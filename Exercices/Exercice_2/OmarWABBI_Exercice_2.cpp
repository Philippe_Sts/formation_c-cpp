#include <iostream>
class Connection
{
public:
    virtual void StartConnection() = 0;
    virtual void StopConnection() = 0;

    const bool ConnectionStatus() const
    {
        return m_ConnectionEstablished;
    }

protected:
    bool m_ConnectionEstablished = false;
};


class ChildConnection : public Connection
{
    public:

        virtual void StartConnection() final{
            std::cout << "Start Connection" << std::endl;
        }

        virtual void StopConnection() final{
            std::cout << "Stop Connection" << std::endl;
        }
};

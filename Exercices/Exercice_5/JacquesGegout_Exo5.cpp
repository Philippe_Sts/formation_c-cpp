#include<iostream>
#include<future>
#include<unistd.h>

using namespace std;

void ThreadFunction(int limit)
{
    for (int i = 0; i<limit; i++)
    {
        cout << i << endl;
        usleep(1000);
    }
        
}

int main(int argc, char** argv) {
    auto a1 = std::async(std::launch::async, ThreadFunction, 500);
    while (true)
    {
        if (a1.wait_for(100ms) == std::future_status::ready)
            break;
        else
            cout << "Still waiting..." << endl;
    }
}
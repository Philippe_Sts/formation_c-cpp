#include <iostream>
#include <future>

int ThreadFunction(int arg_1, int arg_2)
{
    int result = 1;
    for (int i = arg_1; i > 1; i--)
    {
        result *= i;
    }
    return result;
}

int main()
{
    int result = 0;
    std::future<int> threadFn = std::async(std::launch::async, ThreadFunction, 5, 10);
    std::cout << "Result : " << threadFn.get() << std::endl;
    return 0;
}
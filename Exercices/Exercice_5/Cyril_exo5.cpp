#include <iostream>
#include <future>

int ThreadFunction(int arg_1, int arg_2)
{
    int result = 1;
    for(int i = arg_1; i > 1; i--)
    {
        result *= i;
    }

    return result;
}

int main()
{
    int result = 0;
    std::future <int> Async = std::async(std::launch::async, ThreadFunction, 5, 0); 
    result = Async.get();
    std::cout << "Result : " << result << std::endl;
    return 0;
}
#include <iostream>
#include <thread>
#include <future>

int ThreadFunction(int arg_1, int arg_2)
{
    int result = 1;
    for(int i = arg_1; i > 1; i--)
    {
        result *= i;
    }

    return result;
}

int main()
{
    auto future = std::async(ThreadFunction,3,0);
    int result = 0; 
    result = future.get();
    std::cout << "Result : " << result << std::endl;
    return 0;
}
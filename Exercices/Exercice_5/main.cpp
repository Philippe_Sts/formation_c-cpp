#include <iostream>
#include <future>

int ThreadFunction(int arg_1)
{
    int result = 1;
    for(int i = arg_1; i > 1; i--)
    {
        result *= i;
    }

    return result;
}

int main()
{
#pragma region CORRECTION
    std::future<int> async = std::async(std::launch::async, ThreadFunction, 4);

    int result = async.get(); 
#pragma endregion CORRECTION
    std::cout << "Result : " << result << std::endl;
    return 0;
}
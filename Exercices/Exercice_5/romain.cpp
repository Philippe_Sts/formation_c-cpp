#include <iostream>

#include <thread>
#include <future>

int ThreadFunction(int arg_1, int arg_2)
{
    int result = 1;
    for(int i = arg_1; i > 1; i--)
    {
        result *= i;
    }

    return result;
}

int main()
{
    std::future<int> thrd = std::async(ThreadFunction,7,2);
    int result = 0; 
    result = thrd.get();
    std::cout << "Result : " << result << std::endl;
    return 0;
}
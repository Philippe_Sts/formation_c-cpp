#include "Facade.h"
#include "ATMCommand.h"
#include "Proxy.h"
#include "ButtonFactory.h"

int main(int argc, char** argv)
{
    std::shared_ptr<ATM_Facade> facade = std::make_shared<ATM_Facade>();
    std::shared_ptr<ButtonFactory> Factory = std::make_shared<ButtonFactory>(facade);
    MainClientUI ClientUI = MainClientUI(Factory);

    ClientUI.GetCurrentBalance(1234);
    ClientUI.Deposit(1234, 50);
    ClientUI.GetCurrentBalance(1234);
    ClientUI.Withdraw(1234, 100);
    ClientUI.GetCurrentBalance(1234);

    return 0;
}
#pragma once
#include "Facade.h"

class iCMd
{
public:
    virtual ~iCMd(){}

    virtual void Execute() = 0;
    virtual void Unexecute() = 0;
};

class ATM_Cmd_GetBalance : public iCMd 
{
public:
    ATM_Cmd_GetBalance(std::shared_ptr<ATM_Facade> receiver, int& code);

    virtual void Execute() override;
    virtual void Unexecute() override;

private:
    std::shared_ptr<ATM_Facade> m_Receiver;
    int& m_Code;
};

class ATM_Cmd_Withdraw : public iCMd 
{
public:
    ATM_Cmd_Withdraw(std::shared_ptr<ATM_Facade> receiver, int& code, int& amount);

    virtual void Execute() override;
    virtual void Unexecute() override;

private:
    std::shared_ptr<ATM_Facade> m_Receiver;
    int& m_Code;
    int& m_Amount;
};

class ATM_Cmd_Deposit : public iCMd 
{
public:
    ATM_Cmd_Deposit(std::shared_ptr<ATM_Facade> receiver, int& code, int& amount);

    virtual void Execute() override;
    virtual void Unexecute() override;

private:
    std::shared_ptr<ATM_Facade> m_Receiver;
    int& m_Code;
    int& m_Amount;
};
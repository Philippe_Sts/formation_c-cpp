#include "ATMCommand.h"

ATM_Cmd_GetBalance::ATM_Cmd_GetBalance(std::shared_ptr<ATM_Facade> receiver, int& code)
    : m_Receiver(receiver), m_Code(code)
{

}

void ATM_Cmd_GetBalance::Execute()
{
    m_Receiver->GetCurrentBalance(m_Code);
}

void ATM_Cmd_GetBalance::Unexecute()
{

}

ATM_Cmd_Withdraw::ATM_Cmd_Withdraw(std::shared_ptr<ATM_Facade> receiver, int& code, int& amount)
    : m_Receiver(receiver), m_Code(code), m_Amount(amount)
{

}

void ATM_Cmd_Withdraw::Execute()
{
    m_Receiver->Withdraw(m_Code, m_Amount);
}

void ATM_Cmd_Withdraw::Unexecute()
{

}

ATM_Cmd_Deposit::ATM_Cmd_Deposit(std::shared_ptr<ATM_Facade> receiver, int& code, int& amount)
    : m_Receiver(receiver), m_Code(code), m_Amount(amount)
{

}

void ATM_Cmd_Deposit::Execute()
{
    m_Receiver->Deposit(m_Code, m_Amount);
}

void ATM_Cmd_Deposit::Unexecute()
{

}
#include "CheckSecurityCode.h"
#include <unordered_map>
#include <iostream>

namespace
{
    std::unordered_map<int, int> s_CodeDB;
}

CheckSecurityCode::CheckSecurityCode()
{
    s_CodeDB[123456789] = 1234;
}

CheckSecurityCode::~CheckSecurityCode()
{

}

void CheckSecurityCode::SetAccountNbr(int accountNbr)
{
    m_ActiveAccount = accountNbr;
}

bool CheckSecurityCode::CheckCode(int code)
{
    if(s_CodeDB[m_ActiveAccount] == code)
    {
        return true;
    }
    std::cout << "wrong code !\n";
    return false;
}

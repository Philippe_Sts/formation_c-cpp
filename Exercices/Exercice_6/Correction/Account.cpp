#include "Account.h"

Account::Account(int accountNbr,  std::shared_ptr<CheckSecurityCode> checkSecCode)
    : m_AccountNbr(accountNbr), m_CheckSecCode(checkSecCode)
{
    m_CheckSecCode->SetAccountNbr(accountNbr);
    m_Funds = 1000;
}

Account::~Account()
{

}

const int Account::GetAccountNumber() const
{
    return m_AccountNbr;
}

const int Account::GetBlance(int code) const
{
    if(m_CheckSecCode->CheckCode(code))
    {
        return m_Funds;
    }
    return 0;
}

void Account::Withdraw(int amount, int code)
{
    if(m_CheckSecCode->CheckCode(code))
    {
        m_Funds -= amount;
    }
}

bool Account::Deposit(int amount, int code)
{
    if(m_CheckSecCode->CheckCode(code))
    {
        m_Funds += amount;
    }
}
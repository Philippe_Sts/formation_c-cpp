#include "ATM.h"

ATM::ATM()
{
    m_Amount = 5000;
}

ATM::~ATM()
{

}

void ATM::AddCash(int amount)
{
    m_Amount += amount;
}

void ATM::WithdrawCash(int amount)
{
    m_Amount -= amount;
}

int ATM::CheckRemainingCash()
{
    return m_Amount;
}
#pragma once
#include "CheckSecurityCode.h"
#include <memory>

class Account
{
public:
    Account() = delete;
    Account(int accountNbr, std::shared_ptr<CheckSecurityCode> checkSecCode);

    ~Account();

    const int GetAccountNumber() const;

    const int GetBlance(int code) const;

    void Withdraw(int amount, int code);

    bool Deposit(int amount, int code);

private:
    int m_AccountNbr;
    int m_Funds;
    std::shared_ptr<CheckSecurityCode> m_CheckSecCode;
};
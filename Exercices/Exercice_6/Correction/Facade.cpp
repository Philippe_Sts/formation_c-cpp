#include "Facade.h"
#include <iostream>


/*
    Here I pass a delete object to the shared_ptr, [](Account*){}
    Because the Account is stored in the stack, I must tell the shared_ptr not to delete it.
    So the [](Account*){} will be use as destroyer and won't destroy the original variable.
    !!!! if you pass a stack value to a shared_ptr be sure that this stack value life ends after the shared_ptr life.
    
    m_FundsManager(std::shared_ptr<Account>(&m_Account, [](Account*){}), std::shared_ptr<ATM>(&m_ATM, [](ATM*){}))

    The difference with std::make_shared is that std::make_shared will put a copy of the value passed in the heap.
    You can check the difference with the object in m_Account.

    If you need to point to stack object you can use the Raw pointer, 
    it can be a good way to see what is stack allocated and heap using the smart pointers for the heap 
    and raw pointer for stack object.
*/
ATM_Facade::ATM_Facade()
    : m_FundsManager(std::shared_ptr<Account>(&m_Account, [](Account*){}), std::shared_ptr<ATM>(&m_ATM, [](ATM*){})), // last initialized
    m_Account(123456789, std::make_shared<CheckSecurityCode>(m_CheckSecurityCode)), 
    m_CheckSecurityCode(), 
    m_ATM()                 // First initialized
{

}

ATM_Facade::~ATM_Facade()
{

}

void ATM_Facade::GetCurrentBalance(int code) const
{
    std::cout << "Your account balance: " << m_Account.GetBlance(code) << std::endl;
}

void ATM_Facade::Withdraw(int code, int amount)
{
    m_FundsManager.Withdraw(code, amount);
}

void ATM_Facade::Deposit(int code, int amount)
{
    m_FundsManager.Deposit(code, amount);
}

void ATM_Facade::AddCash(int amount)
{
    m_ATM.AddCash(amount);
}

void ATM_Facade::ATMCheckRemainingCash()
{
    std::cout << "Remaining cash: " << m_ATM.CheckRemainingCash() << std::endl;
}
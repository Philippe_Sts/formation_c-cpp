#include "FundsManager.h"
#include <iostream>

FundsManager::FundsManager(std::shared_ptr<Account> account, std::shared_ptr<ATM> atm)
    : m_Account(account), m_ATM(atm)
{

}

FundsManager::~FundsManager()
{

}

void FundsManager::GetCurrentBalance(int code) const
{
    std::cout << "Your current balance is: " <<  m_Account->GetBlance(code) << std::endl;
}

bool FundsManager::Withdraw(int code, int cashToWithdraw)
{
    if(m_ATM->CheckRemainingCash() > cashToWithdraw && m_Account->GetBlance(code) > cashToWithdraw)
    {
        m_ATM->WithdrawCash(cashToWithdraw);
        m_Account->Withdraw(cashToWithdraw, code);
        return true;
    }
    std::cout << "not enought money\n"; 
    return false;
}

void FundsManager::Deposit(int code, int cashToDeposit)
{
    if(m_Account->Deposit(cashToDeposit, code))
    {
        m_ATM->AddCash(cashToDeposit);
    }
}

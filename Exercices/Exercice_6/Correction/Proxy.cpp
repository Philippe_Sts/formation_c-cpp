#include "Proxy.h"

MainClientUI::MainClientUI(std::shared_ptr<ButtonFactory> factory)
{
    m_GetCurrentBalanceButton = factory->GetGetCurrencyButton(m_EnteredCode);
    m_WithdrawButton = factory->GetWithdrawButton(m_EnteredCode, m_EnteredAmount);
    m_DepositButton = factory->GetDepositButton(m_EnteredCode, m_EnteredAmount);
}

void MainClientUI::GetCurrentBalance(int code)
{
    m_EnteredCode = code;
    m_GetCurrentBalanceButton->Clicked();
}

void MainClientUI::Withdraw(int code, int amount)
{
    m_EnteredCode = code;
    m_EnteredAmount = amount;
    m_WithdrawButton->Clicked();
}

void MainClientUI::Deposit(int code, int amount)
{
    m_EnteredCode = code;
    m_EnteredAmount = amount;
    m_DepositButton->Clicked();
}
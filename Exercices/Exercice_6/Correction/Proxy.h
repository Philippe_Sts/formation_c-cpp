#pragma once

#include "ATMCommand.h"
#include "Facade.h"
#include "ButtonFactory.h"

class ClientProxy
{
public:
    virtual ~ClientProxy(){}

    virtual void GetCurrentBalance(int code) = 0;
    virtual void Withdraw(int code, int amount) = 0;
    virtual void Deposit(int code, int amount) = 0;
};

class MainClientUI : public ClientProxy
{
public:
    MainClientUI(std::shared_ptr<ButtonFactory> factory);

    ~MainClientUI(){}

    virtual void GetCurrentBalance(int code) override;
    virtual void Withdraw(int code, int amount) override;
    virtual void Deposit(int code, int amount) override;
    
protected:
    std::shared_ptr<iButton> m_GetCurrentBalanceButton;
    std::shared_ptr<iButton> m_WithdrawButton;
    std::shared_ptr<iButton> m_DepositButton;

    int m_EnteredCode = 0;
    int m_EnteredAmount = 0;
};
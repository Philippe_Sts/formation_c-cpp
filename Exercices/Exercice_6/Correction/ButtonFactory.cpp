#include "ButtonFactory.h"

GetCurrencyButton::GetCurrencyButton(std::shared_ptr<ATM_Facade> facade, int& code)
{
    m_Command = std::make_shared<ATM_Cmd_GetBalance>(facade, code);
}

GetCurrencyButton::~GetCurrencyButton()
{

}

void GetCurrencyButton::Clicked()
{
    m_Command->Execute();
}

WithdrawButton::WithdrawButton(std::shared_ptr<ATM_Facade> facade, int& code, int& amount)
{
    m_Command = std::make_shared<ATM_Cmd_Withdraw>(facade, code, amount);
}

WithdrawButton::~WithdrawButton()
{

}

void WithdrawButton::Clicked()
{
    m_Command->Execute();
}

DepositButton::DepositButton(std::shared_ptr<ATM_Facade> facade, int& code, int& amount)
{
    m_Command = std::make_shared<ATM_Cmd_Deposit>(facade, code, amount);
}

DepositButton::~DepositButton()
{

}

void DepositButton::Clicked()
{
    m_Command->Execute();
}

ButtonFactory::ButtonFactory(std::shared_ptr<ATM_Facade> facade)
    : m_Facade(facade)
{

}

ButtonFactory::~ButtonFactory()
{

}

std::shared_ptr<iButton> ButtonFactory::GetGetCurrencyButton(int& code)
{
    return std::make_shared<GetCurrencyButton>(m_Facade, code);
}

std::shared_ptr<iButton> ButtonFactory::GetWithdrawButton(int& code, int& amount)
{
    return std::make_shared<WithdrawButton>(m_Facade, code, amount);
}

std::shared_ptr<iButton> ButtonFactory::GetDepositButton(int& code, int& amount)
{
    return std::make_shared<DepositButton>(m_Facade, code, amount);
}

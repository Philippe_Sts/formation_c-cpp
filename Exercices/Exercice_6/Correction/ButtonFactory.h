#pragma once

#include "ATMCommand.h"
#include <memory>

class iButton
{
public:
    virtual ~iButton() {}

    virtual void Clicked() = 0;

protected:
    std::shared_ptr<iCMd> m_Command;
};

class GetCurrencyButton : public iButton
{
public:
    GetCurrencyButton() = delete;
    GetCurrencyButton(std::shared_ptr<ATM_Facade> facade, int& code);
    ~GetCurrencyButton();

    virtual void Clicked() override;
};

class WithdrawButton : public iButton
{
public:
    WithdrawButton() = delete;
    WithdrawButton(std::shared_ptr<ATM_Facade> facade, int& code, int& amount);
    ~WithdrawButton();

    virtual void Clicked() override;
};

class DepositButton : public iButton
{
public:
    DepositButton() = delete;
    DepositButton(std::shared_ptr<ATM_Facade> facade, int& code, int& amount);
    ~DepositButton();

    virtual void Clicked() override;
};

class ButtonFactory
{
public:
    ButtonFactory() = delete;
    ButtonFactory(std::shared_ptr<ATM_Facade> facade);
    ~ButtonFactory();

    std::shared_ptr<iButton> GetGetCurrencyButton(int& code);
    std::shared_ptr<iButton> GetWithdrawButton(int& code, int& amount);
    std::shared_ptr<iButton> GetDepositButton(int& code, int& amount);

private:
    std::shared_ptr<ATM_Facade> m_Facade;
};
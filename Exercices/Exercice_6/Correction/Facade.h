#pragma once
#include "ATM.h"
#include "CheckSecurityCode.h"
#include "Account.h"
#include "FundsManager.h"


class ATM_Facade
{
public:
    ATM_Facade();
    ~ATM_Facade();

    /*
    Return the current balance.
    */
    void GetCurrentBalance(int code) const;

    /*
    Withdraw cash
    */
    void Withdraw(int code, int amount);

    /*
    Deposit cash to the account.
    */
    void Deposit(int code, int amount);

    /*
    Add cash to the ATM
    */
    void AddCash(int amount);

    void ATMCheckRemainingCash();

private:
    /*
    Manage the current amount of cash in the ATM
        contain:
            1 int = amount of cash in the ATM
            functions to add or remove cash of the ATM
            functions to check the remaining cash and check if the user can have the cash he asked for.
    */
    ATM m_ATM;

    /*
    check the security code passed by the client
    contain:
        1 static (or unnamed namespace) map with Account Nbr as key and account "code" as value.
        1 function that fetch the code of a given account FetchCode(int accountNbr)
        functions that check if the code is correct
    */
    CheckSecurityCode m_CheckSecurityCode;

    /*
    Manage the client account
    require:
        1 CheckSecurityCode*
    */
    Account m_Account;

    /*
    Manage the blance of the account.
    require :
        1 ATM*
        1 Account*
    */
    FundsManager m_FundsManager;
};
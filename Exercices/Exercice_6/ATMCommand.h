#pragma once
#include "Facade.h"

class iCMd
{
public:
    virtual ~iCMd(){}

    virtual void Execute() = 0;
    virtual void Unexecute() = 0;
};

class ATM_Cmd_GetBalance : public iCMd {
public:
    ATM_Cmd_GetBalance(ATM_Facade *receiver);

    virtual void Execute() override;
    virtual void Unexecute() override;

private:
    ATM_Facade* m_Receiver;
};
#include <math.h>
#include <iostream>
#include <functional>

class Event
{
    public:
    Event(){}
    ~Event(){}

    static void Print(int n){
         std::cout << "Random got after " << n <<  " tries" << std::endl;
    }
    private:

};

class API
{
public:
    API(): m_uCount(0){
        SetEventFunction(Event::Print);
    }
    ~API(){}

    void SetEventFunction(std::function<void(const int)> a_Function){
        EventFunction = std::bind(a_Function, std::placeholders::_1);
    }

    void MainLoop()
    {
        while(1)
        {
            uint nbr = rand() % 1000000;
            if(nbr == 7)
            {
                // std::cout << "Debug " << m_uCount << std::endl;
                //exec event function
                EventFunction(m_uCount);
            }
            m_uCount++;
        }
    }
private:
    unsigned int m_uCount;
    std::function<void(int)> EventFunction;
};

int main()
{
    API api;
    api.MainLoop();

    return 0;
}
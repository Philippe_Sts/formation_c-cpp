#include <math.h>
#include <iostream>
#include <functional>

// 1ère version

class Event
{
    public:
        static void MyEventNoArgs(){
            std::cout<<"Coucou"<< std::endl;
        };        
        
        static void MyEventWithArgs(std::string sName){
            std::cout<<"Coucou " << sName << std::endl;
        };
};

class API
{
public:
    API(){}
    ~API(){}

    void SetEventFunction(std::function<void()> pFunction){
        MyFunction = pFunction;

    }

    void SetEventFunction2(std::function<void(std::string)> pFunction){
        MyFunctionWithArgs = std::bind(pFunction, std::placeholders::_1);

    }
    
    void MainLoop()
    {
        SetEventFunction(Event::MyEventNoArgs);
        SetEventFunction2(Event:: MyEventWithArgs);
        
        while(1)
        {
            uint nbr = rand() % 100000000;
            if(nbr > 7)
            {
                MyFunction();
                MyFunctionWithArgs("Pierre");
            }
        }
    }
private:
     std::function<void()> MyFunction;
     std::function<void(std::string)>  MyFunctionWithArgs;      
};

int main()
{
    API api;
    api.MainLoop();

    return 0;
}
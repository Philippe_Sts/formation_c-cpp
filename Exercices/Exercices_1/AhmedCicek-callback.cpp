#include <math.h>
#include <iostream>

typedef void (*callbackFunction)(void);

class Event
{
  public:
	static void printEvent(){
		std::cout << "An event is raised!";
	}
};

class API
{
public:
    API(){}
    ~API(){}

    void SetEventFunction(callbackFunction eventFunction){
    	ptrFunction = eventFunction;
    }

    void MainLoop()
    {
        while(1)
        {
            uint nbr = rand() % 100000000;
            if(nbr == 7)
            {
                ptrFunction();
            }
        }
    }
private:
    callbackFunction ptrFunction;
};

int main()
{
    API api;

    api.SetEventFunction(Event::printEvent);
    api.MainLoop();

    return 0;
}

#include <math.h>
#include <iostream>
#include <functional>
#include <boost/function.hpp>
#include <boost/bind.hpp>

typedef boost::function<void(const char*)> EventFct;
#define BIND_EVENT_FCT(x) boost::bind(&x, _1)
#define BIND_MBR_EVENT_FCT(x) boost::bind<void>(&x, this, _1)

class Event
{
public:
    Event(){}
    ~Event(){}

    EventFct GetEventFct()
    {
        return BIND_MBR_EVENT_FCT(Event::ExecEvent);
    }

    static void StaticExecEvent(const char* str) 
    {
        std::cout << str << std::endl;
    }

protected:
    virtual void ExecEvent(const char* str) 
    {
        std::cout << str << std::endl;
    }
};

class Event2 : public Event
{
public:
    Event2(){}
    ~Event2(){}

protected:
    virtual void ExecEvent(const char* str) override
    {
        std::cout << "override function " << str << std::endl;
    }

};

class API
{
public:
    API(){}
    ~API(){}

    void SetEventFunction(EventFct e_fct)
    {
        m_EventFct = e_fct;
    }

    void SetEventFct(Event& e)
    {
        m_EventFct = e.GetEventFct();
    }

    void MainLoop()
    {
        while(1)
        {
            uint nbr = rand() % 100000000;
            if(nbr == 7)
            {
                m_EventFct("string");
            }
        }
    }
private:
    EventFct m_EventFct;
};

int main()
{
    Event2 e;
    API api;
    api.SetEventFunction(BIND_EVENT_FCT(Event::StaticExecEvent));
    api.SetEventFunction(e.GetEventFct());
    api.SetEventFct(e);
    api.MainLoop();

    return 0;
}
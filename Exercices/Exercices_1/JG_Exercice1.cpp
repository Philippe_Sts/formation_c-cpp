#include <math.h>
#include <functional>
#include <iostream>
#include <unistd.h>

typedef int (*fnGetNumber)(int);

class Event
{
    public:
    static int GetMeANumber(int ZaMakushimumo)
    {
        return rand() % ZaMakushimumo;
    }
};

class API
{
public:
    API(){}
    ~API(){}

    inline void SetEventFunction(std::function<int(int)> aFunc){
        EventCallback = std::bind(aFunc, std::placeholders::_1);
    }

    void MainLoop(int max)
    {
        while(1)
        {
            uint nbr = rand() % 10;
            if(nbr == 7)
            {
                int a = EventCallback(max);
                std::cout << a << std::endl;
                usleep(1000*1000);
            }
            
        }
    }
private:
    std::function<int(int)> EventCallback;
};

int main(int argc, char** argv)
{
    srand(time(NULL));
    API api;
    api.SetEventFunction(Event::GetMeANumber);
    int n = argc > 1 ? std::stoi(argv[1]) : 1000;
    std::cout << argc << " n = " << n << std::endl;
    api.MainLoop(n);

    return 0;
}
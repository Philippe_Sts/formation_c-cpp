#include <math.h>
#include <iostream>

typedef void (*callback)(void);

class Event
{
    public:
        static void DoThing()
        {
            std::cout << "Thing successfully done" << std::endl;
        }
};

class API
{
public:
    API(){}
    ~API(){}

    void SetEventFunction(callback f){
        std::cout << "Callback called" << std::endl;
        f();
    }

    void MainLoop()
    {
        while(1)
        {
            uint nbr = rand() % 100000000;
            if(nbr == 7)
            {
                //exec event function
                SetEventFunction(Event::DoThing);
            }
        }
    }
private:
};

int main()
{
    API api;
    api.MainLoop();

    return 0;
}